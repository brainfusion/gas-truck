/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * <p>
 * http://aws.amazon.com/apache2.0
 * <p>
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.session_credentials.Utilities;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;

/**
 * A class which performs the task of authentication the user. For the sample it
 * validates a set of username and possword against the sample Cognito developer
 * authentication application
 */
public class DeveloperAuthenticationTask extends AsyncTask<LoginCredentials, Void, Void> {

    // The user name or the developer user identifier you will pass to the
    // Amazon Cognito in the GetOpenIdTokenForDeveloperIdentity API
    private String userName;
    private CloudSyncCallback cloudSyncCallback;
    final String AWS_DEVICE_UID = "AWS_DEVICE_UID";
    static final String AWS_DEVICE_TOKEN = "AWS_DEVICE_TOKEN";
    private boolean isSuccessful;
    private Response response=null;

    private final Context context;
    public DeveloperAuthenticationTask(Context context, CloudSyncCallback cloudSyncCallback) {
        this.context = context;
        this.cloudSyncCallback = cloudSyncCallback;
    }

    @Override
    protected Void doInBackground(LoginCredentials... params) {
        response = DeveloperAuthenticationProvider.getDevAuthClientInstance().login(params[0].getUsername(), params[0].getPassword(),params[0].getTypeSession());
        String token = "";
        String identityId = "";
        Log.d("AWS", " RESPONSE: " + response.getResponseMessage());

        isSuccessful = response.requestWasSuccessful();
        userName = params[0].getUsername();

        if (isSuccessful && Utilities.getToken(response.getResponseMessage())!=null) {
            CognitoSyncClientManager.addLogins(((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider
                            .getIdentityProvider()).getProviderName(),
                    userName);
            token = Utilities.getToken(response.getResponseMessage());
            identityId = Utilities.getIdentityId(response.getResponseMessage());
            //Save Token in Shared Preferences
            AmazonSharedPreferencesWrapper.storeValueInSharedPreferences(PreferenceManager
                    .getDefaultSharedPreferences(context), AWS_DEVICE_TOKEN, token);
            AmazonSharedPreferencesWrapper.storeValueInSharedPreferences(PreferenceManager
                    .getDefaultSharedPreferences(context), AWS_DEVICE_UID, identityId);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        cloudSyncCallback.onLoginSuccess(response);
        if (!isSuccessful) {
        }
    }
}