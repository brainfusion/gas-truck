package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.management.GaszenManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.OrderData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by carlosrodriguez on 2/7/17.
 */

public class OrderInfo extends android.support.v4.app.Fragment implements CloudSyncCallback {

    private Typeface regularFont = null, boldFont = null;
    private GaszenDB_DTO db = null;
    private GaszenManager gaszenManager = null;
    private List<String> orderData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int graphColorTank = Color.argb(255, 64, 196, 0);
        int backgroundColor = Color.argb(255, 218, 218, 218);
        int appBackgroundColor = Color.argb(255, 244, 244, 248);

        View orderInfo = inflater.inflate(R.layout.fragment_order_information, container, false);
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");
        db = db.getInstance(getContext());
        gaszenManager = new GaszenManager(getContext());

        int idOrder = getArguments().getInt("order");

        OrderData data = db.GetOrderInfo(idOrder);
        orderData = db.GetStatusInfo(idOrder);

        ImageView imgvDone = (ImageView) orderInfo.findViewById(R.id.imgv_done);
        ImageView imgvCancel = (ImageView) orderInfo.findViewById(R.id.imgv_cancel);
        TextView txvAddress1 = (TextView) orderInfo.findViewById(R.id.txv_order_address1);
        txvAddress1.setTypeface(regularFont);
        String address = data.getClient().getClient_street_address() + " #" + data.getClient().getClient_ext_number();
        txvAddress1.setText(address);
        TextView txvAddress2 = (TextView) orderInfo.findViewById(R.id.txv_order_address2);
        txvAddress2.setTypeface(regularFont);
        txvAddress2.setText(data.getClient().getClient_neighborhood_address());
        TextView txvName = (TextView) orderInfo.findViewById(R.id.txv_order_name);
        txvName.setTypeface(regularFont);
        txvName.setText(data.getClient().getClient_name());
        TextView txvCity = (TextView) orderInfo.findViewById(R.id.txv_order_city);
        txvCity.setTypeface(regularFont);
        txvCity.setText(data.getClient().getClient_city());
        TextView txvPCode = (TextView) orderInfo.findViewById(R.id.txv_order_postal);
        txvPCode.setTypeface(regularFont);
        txvPCode.setText(data.getClient().getClient_postal_code().toString());
        TextView txvQuantity = (TextView) orderInfo.findViewById(R.id.txv_quatity);
        txvQuantity.setTypeface(regularFont);
        TextView txvCoin = (TextView) orderInfo.findViewById(R.id.txv_coin);
        txvCoin.setTypeface(regularFont);

        TextView edtCoin = (TextView) orderInfo.findViewById(R.id.edt_coin);
        edtCoin.setTypeface(regularFont);
        edtCoin.setText(data.getOrder().getTotal_sale().toString());

        TextView txvLiter = (TextView) orderInfo.findViewById(R.id.txv_liter);
        txvLiter.setTypeface(regularFont);

        TextView edtLiter = (TextView) orderInfo.findViewById(R.id.edt_liter);
        edtLiter.setTypeface(regularFont);
        edtLiter.setText(data.getOrder().getQty_liters().toString());

        TextView txvPrice = (TextView) orderInfo.findViewById(R.id.txv_price);
        txvPrice.setTypeface(regularFont);
        TextView txvPriceAnon = (TextView) orderInfo.findViewById(R.id.txv_price_anon);
        txvPriceAnon.setTypeface(regularFont);
        txvPriceAnon.setText(data.getOrder().getPrice_per_liter().toString());
        TextView txvCurrency = (TextView) orderInfo.findViewById(R.id.txv_currency);
        txvCurrency.setTypeface(regularFont);
        txvCurrency.setText(data.getOrder().getCurrency());
        TextView txvMethod = (TextView) orderInfo.findViewById(R.id.txv_method);
        txvMethod.setTypeface(regularFont);

        String paymentMethod = data.getOrder().getPayment_method();
        TextView txvPaymentMethod = (TextView) orderInfo.findViewById(R.id.txv_payment_method);
        txvPaymentMethod.setTypeface(boldFont);
//        txvPaymentMethod.setText(data.getOrder().getPayment_method());

        switch (paymentMethod) {
            case "CASH-OTHER":
                txvPaymentMethod.setText(R.string.order_info_payment_cash);
                break;
            case "CREDITCARD_OPENPAY":
                txvPaymentMethod.setText(R.string.order_info_payment_credit_card);
                break;

        }

        TextView txvFinalize = (TextView) orderInfo.findViewById(R.id.txv_finalize_order);
        txvFinalize.setTypeface(boldFont);

        final TextView txvTankLvl = (TextView) orderInfo.findViewById(R.id.txv_tank_lvl);
        txvTankLvl.setTypeface(regularFont);
        final TextView txvTankCap = (TextView) orderInfo.findViewById(R.id.txv_tank_cap);
        txvTankCap.setTypeface(regularFont);
        txvTankCap.setText(getString(R.string.status_tank_capacity) + ": " + data.getTank().getTank_capacity());
        final TextView txvTankLvlTxt = (TextView) orderInfo.findViewById(R.id.txv_tank_lvl_text);
        txvTankLvlTxt.setTypeface(regularFont);

        //Series Item for background of each graph
        SeriesItem seriesItemBackground = new SeriesItem.Builder(appBackgroundColor)
                .setRange(99, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .setInterpolator(new LinearInterpolator())
                .setShowPointWhenEmpty(false)
                .setInset(new PointF(100f, 100f))
                .setCapRounded(false)
                .setDrawAsPoint(false)
                .setSpinClockwise(true)
                .setSpinDuration(6000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();


        DecoView arcView = (DecoView) orderInfo.findViewById(R.id.dynamicArcView_tank_level);
        arcView.configureAngles(180, 0);
        int tankValue = data.getTank().getTank_actual_level();

        double tankPercentage = (data.getTank().getTank_actual_level() * 100) / data.getTank().getTank_capacity();

        if (tankPercentage >= 50) {
            graphColorTank = Color.argb(255, 64, 196, 0);
        } else if (tankPercentage < 50 && tankPercentage > 20) {
            graphColorTank = Color.argb(255, 255, 255, 51);
        } else if (tankPercentage < 20) {
            graphColorTank = Color.argb(255, 255, 0, 0);
        }
        // Create background track
        arcView.addSeries(new SeriesItem.Builder(backgroundColor)
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .build());

        //Create data series track
        final SeriesItem seriesItem1 = new SeriesItem.Builder(graphColorTank)
                .setRange(0, data.getTank().getTank_capacity(), 0)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(false)
                .setDrawAsPoint(false)
                .setInset(new PointF(2f, 2f))
                .setSpinClockwise(true)
                .setSpinDuration(7000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();

        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                txvTankLvl.setText((String.format("%.0f%%", percentFilled * 100f)));

            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        int series1Index = arcView.addSeries(seriesItem1);
        int seriesBackgroundIndex = arcView.addSeries(seriesItemBackground);


        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        arcView.addEvent(new DecoEvent.Builder(tankValue).setIndex(series1Index).setDelay(1000).build());
        //arcView.addEvent(new DecoEvent.Builder(100).setIndex(series11Index).setDuration(0).build());

        arcView.addEvent(new DecoEvent.Builder(110).setIndex(seriesBackgroundIndex).build());

        imgvDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(v, true);
            }
        });

        imgvCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(v, false);
            }
        });

        return orderInfo;
    }

    private boolean checkConnectivity() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            //Toast.makeText(this, getString(R.string.abc_message_no_intenet), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onLoginSuccess(Response success) {

    }

    @Override
    public void onRequestSuccess(String data, short id) {
        try {
            if (id == 0) {
                Log.d("JSON", data);
                String format = data.replace("\\", "");
                format = format.substring(1, format.length() - 1);
                Log.d("JSON", "Data FORMAT: " + format);
                JSONObject jsonData = new JSONObject(format);
                int resultCode = Integer.parseInt(jsonData.getString("result_code"));
                if (resultCode != 200) {
                    Toast.makeText(getContext(), R.string.dialog_update_error, Toast.LENGTH_LONG).show();
                } else if (resultCode == 200) {
                    Toast.makeText(getContext(), R.string.dialog_update_success, Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostSuccess(String serverResponse) {

    }

    @Override
    public void onPostException(String error) {

    }

    public void showDialog(View v, final boolean flag) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.setContentView(R.layout.custom_dialog_update_order);

        ImageView icon = (ImageView) dialog.findViewById(R.id.icondialog);
        icon.setImageResource(R.mipmap.ic_about);

        TextView title = (TextView) dialog.findViewById(R.id.txvtitledialog);
        title.setTypeface(boldFont);
        title.setText(flag ? R.string.done_dialog_title : R.string.cancel_dialog_title);

        final EditText message = (EditText) dialog.findViewById(R.id.edtdialog);
        message.setTypeface(regularFont);
        message.setTextColor(Color.BLACK);
        message.setHint(flag ? R.string.done_dialog_notes : R.string.cancel_dialog_notes);
        //message.setFocusable(flag ? true : false);

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.btndialogok);
        dialogButtonOk.setTextColor(Color.BLACK);
        dialogButtonOk.setTypeface(boldFont);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    boolean flagCon = checkConnectivity();
                    if (flagCon) {
                        JSONObject json = new JSONObject();
                        json.put("sale_status", flag ? "COMPLETED" : "CANCELLED");
                        json.put("device_uuid", orderData.get(0));
                        json.put("timestamp", orderData.get(1));
                        json.put("notes", flag ? "" : message.getText().toString());
                        gaszenManager.UpdateOrderStatusGM(json.toString(), (short) 0, OrderInfo.this);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getContext(), R.string.toast_connection_error, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getContext(), R.string.toast_update_order_error, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });

        Button dialogButtonDecline = (Button) dialog.findViewById(R.id.btndialogdecline);
        dialogButtonDecline.setTextColor(Color.BLACK);
        dialogButtonDecline.setTypeface(boldFont);

        dialogButtonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
