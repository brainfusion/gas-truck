package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.view.Home2;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.OrderAddress;

import java.util.List;
import java.util.Random;

/**
 * Created by carlosrodriguez on 1/10/17.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private Typeface boldFont = null, regularFont = null;
    private List<OrderAddress> mDataset;
    private Fragment reference;
    private int position;
    private Context mContext;

    //This class is the adapter for the order list

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView street, col;
        public ImageView icon;
        public ImageView imgdone;

        // Create an instance of the dialog fragment
        public ViewHolder(View v) {
            super(v);
            street = (TextView) v.findViewById(R.id.txvStreet);
            col = (TextView) v.findViewById(R.id.txvCol);
            icon = (ImageView) v.findViewById(R.id.listicon);
            imgdone = (ImageView) v.findViewById(R.id.listdone);
            //imgcancel = (ImageView) v.findViewById(R.id.listcancel);

            //Sets On Click Listener for the view and both imageviews
            v.setOnClickListener(this);
            imgdone.setOnClickListener(this);
            //imgcancel.setOnClickListener(this);
        }

        //On Click Listener for view
        @Override
        public void onClick(View v) {
            position = Integer.valueOf(getAdapterPosition());

            if (v.getId() == imgdone.getId()) {
                //mDataset.remove(position);
                //notifyItemRemoved(position);
                //showDialog(v, true);
                if(mContext instanceof Home2){
                    int a = mDataset.get(position).getId_order();
                    ((Home2)mContext).showFragment(7, a);
                }
            } else {
                //Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrdersAdapter(List<OrderAddress> myDataset, Fragment fragment, Context context) {
        mDataset = myDataset;
        reference = fragment;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        boldFont = Typeface.createFromAsset(v.getContext().getAssets(), "Nexa-Bold.otf");
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(v.getContext().getAssets(), "LatoRegular.ttf");
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        OrderAddress orderAddress = mDataset.get(position);
        holder.street.setText(orderAddress.getClient_street_address());
        holder.col.setText(orderAddress.getClient_neighborhood_address());
        Random r = new Random();
        int num = r.nextInt(5);
        if (num <= 2) {
            holder.icon.setImageResource(R.mipmap.ic_home_black);
        } else {
            holder.icon.setImageResource(R.mipmap.ic_house);
        }
        holder.street.setTypeface(boldFont);
        holder.col.setTypeface(boldFont);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    /*public void showDialog(View v, boolean choice) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.setContentView(R.layout.custom_dialog);

        ImageView icon = (ImageView) dialog.findViewById(R.id.icondialog);
        icon.setImageResource(R.mipmap.ic_about);

        TextView title = (TextView) dialog.findViewById(R.id.txvtitledialog);
        title.setTypeface(boldFont);
        title.setText(R.string.dialog_title);

        TextView message = (TextView) dialog.findViewById(R.id.txvdialog);
        message.setTypeface(regularFont);
        if (choice) {
            message.setText(R.string.dialog_finish);
        } else if (!choice) {
            message.setText(R.string.dialog_cancel);
        }

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.btndialogok);
        dialogButtonOk.setTypeface(boldFont);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), R.string.dialog_accept, Toast.LENGTH_SHORT).show();
                mDataset.remove(position);
                notifyItemRemoved(position);
                dialog.dismiss();
            }
        });

        Button dialogButtonDecline = (Button) dialog.findViewById(R.id.btndialogdecline);
        dialogButtonDecline.setTypeface(boldFont);

        dialogButtonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), R.string.dialog_decline, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
    }*/

}
