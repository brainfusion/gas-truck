package com.tanktruck.gaszen.brainfusion.tanktrunk.controller;

import android.content.Context;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Client;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Order;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Tank;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by carlosrodriguez on 3/27/17.
 */

public class ParserJSON {

    private GaszenDB_DTO db = null;

    public long parseOrdersData(JSONObject jsonData, Context context) {
        db = GaszenDB_DTO.getInstance(context);
        db.ClearOrders();
        long result = 0;
        try {
            JSONArray jsonArray = jsonData.getJSONArray("result_contents");

            for (int i = 0; i < jsonArray.length(); i++) {
                Client client = new Client();
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                JSONObject jsonClient = jsonObjects.getJSONObject("client");

                client.setClient_street_address(jsonClient.getString("client_street_address"));
                client.setClient_ext_number(jsonClient.getInt("client_ext_number"));
                client.setClient_name(jsonClient.getString("client_name"));
                client.setClient_int_number(jsonClient.getInt("client_int_number"));
                client.setClient_postal_code(Integer.parseInt(jsonClient.getString("client_postal_code")));
                client.setClient_city(jsonClient.getString("client_city"));
                client.setClient_neighborhood_address(jsonClient.getString("client_neighborhood_address"));

                //Lat and lng
                client.setClient_lat(Double.parseDouble(jsonClient.getString("latitude")));
                client.setClient_lng(Double.parseDouble(jsonClient.getString("longitude")));

                Tank tank = new Tank();
                JSONObject jsonTank = jsonObjects.getJSONObject("tank");
                tank.setTank_actual_level(jsonTank.getInt("tank_actual_level"));
                tank.setTank_capacity(jsonTank.getInt("tank_capacity"));

                Order order = new Order();
                JSONObject jsonOrder = jsonObjects.getJSONObject("order");
                order.setQty_liters(jsonOrder.getString("qty_liters"));
                order.setPayment_method(jsonOrder.getString("payment_method"));
                order.setSale_status(jsonOrder.getString("sale_status"));
                order.setPrice_per_liter(jsonOrder.getString("price_per_liter"));
                order.setCurrency(jsonOrder.getString("currency"));
                order.setTotal_sale(jsonOrder.getString("total_sale"));
                order.setTimestamp(jsonOrder.getString("timestamp"));
                order.setDevice_uuid(jsonOrder.getString("device_uuid"));
                order.setOrder_id(jsonOrder.getString("order_id"));
                order.setPayment_method_authorization(jsonOrder.getString("payment_method_authorization"));
                order.setSale_payment_status(jsonOrder.getString("sale_payment_status"));
                order.setEmail(jsonOrder.getString("email"));
                order.setPayment_method_id(jsonOrder.getString("payment_method_id"));

                result = db.saveOrdersData(client, tank, order);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result;
    }

}
