package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.tanktruck.gaszen.brainfusion.tanktrunk.Manifest;
import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.ParserJSON;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.adapter.OrdersAdapter;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.AmazonSharedPreferencesWrapper;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.management.GaszenManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Order;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.OrderAddress;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by carlosrodriguez on 1/9/17.
 */

public class Orders extends Fragment implements CloudSyncCallback, OnMapReadyCallback {

    private Typeface regularFont = null, boldFont = null;
    private ParserJSON parserJSON;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    //    private static TextView test;
    private GaszenManager gaszenManager = null;
    private JSONObject jsonData;
    private GaszenDB_DTO db = null;
    List<OrderAddress> myDataset;
    List<LatLng> orderLocations;
    List<Marker> originMarkers = new ArrayList<>();
    List<Marker> destinationsMarkers = new ArrayList<>();
    SupportMapFragment mapFragment;
    private int MY_PERMISSIONS_REQUEST_LOCATION;

    private Dialog customDialog;
    private Button dialogLeftButton;
    private Button dialogRightButton;
    private TextView dialogTitle;
    private TextView dialogBody;
    private ImageView dialogImage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View orders = inflater.inflate(R.layout.fragment_orders, container, false);

        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");

        parserJSON = new ParserJSON();
        gaszenManager = new GaszenManager(getContext());
        db = db.getInstance(getContext());
//        test = (TextView) orders.findViewById(R.id.txvtest);

        //We check connectivity and if there is we get orders

        boolean flag = checkConnectivity();
        if (flag) {
            gaszenManager.GetOrdersGM("", (short) 0, Orders.this);
        } else {
            Toast.makeText(getContext(), R.string.toast_connection_error, Toast.LENGTH_LONG).show();
        }

        recyclerView = (RecyclerView) orders.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) orders.findViewById(R.id.progress_bar_orders);

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }).start();

        //Google Maps Fragement Implementation
//        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
//                .findFragmentById(R.id.mapFragment);

        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        //Custom Dialog Components
        customDialog = new Dialog(getContext());
        customDialog.setContentView(R.layout.custom_alert_dialog);
        dialogLeftButton = (Button) customDialog.findViewById(R.id.btnAlertLeft);
        dialogLeftButton.setTypeface(boldFont);
        dialogRightButton = (Button) customDialog.findViewById(R.id.btnAlertRight);
        dialogRightButton.setTypeface(boldFont);
        dialogTitle = (TextView) customDialog.findViewById(R.id.txtAlertTitle);
        dialogTitle.setTypeface(boldFont);
        dialogBody = (TextView) customDialog.findViewById(R.id.txtAlertBody);
        dialogBody.setTypeface(regularFont);
        dialogImage = (ImageView) customDialog.findViewById(R.id.iconAlertDialog);
        dialogImage.setImageResource(R.mipmap.ic_alert);
        dialogRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        return orders;
    }

    private boolean checkConnectivity() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            //Toast.makeText(this, getString(R.string.abc_message_no_intenet), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onLoginSuccess(Response success) {

    }

    @Override
    public void onRequestSuccess(String data, short id) {
        try {
            if (id == 0) {
                Log.d("JSON", data);
                String format = data.replace("\\", "");
                format = format.substring(1, format.length() - 1);
                Log.d("JSON", "DATA FORMAT: " + format);
                jsonData = new JSONObject(format);
                int resultCode = Integer.parseInt(jsonData.getString("result_code"));
//                JSONArray orders = jsonData.getJSONArray("result_contents");


                switch (resultCode) {
                    case 200:
                        long response = parserJSON.parseOrdersData(jsonData, getContext());
                        ArrayList<LatLng> ordersCoord = new ArrayList<>();
                        if (response != -1) {
                            myDataset = db.GetOrdersAddress();
                            mAdapter = new OrdersAdapter(myDataset, this, getContext());
                            mLayoutManager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(getContext(), R.string.toast_orders_error + ", last id:" + response, Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 6000:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_6000_title);
                        dialogBody.setText(R.string.order_error_6000_body);
                        customDialog.show();
                        break;
                    case 6001:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_6001_title);
                        dialogBody.setText(R.string.order_error_6001_body);
                        customDialog.show();
                        break;
                    case 6002:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_6002_title);
                        dialogBody.setText(R.string.order_error_6002_body);
                        customDialog.show();
                        break;
                    case 5029:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_5029_title);
                        dialogBody.setText(R.string.order_error_5029_body);
                        customDialog.show();
                        break;
                    case 5030:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_5030_title);
                        dialogBody.setText(R.string.order_error_5030_body);
                        customDialog.show();
                        break;
                    case 5031:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_5031_title);
                        dialogBody.setText(R.string.order_error_5031_body);
                        customDialog.show();
                        break;
                    case 5032:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_5032_title);
                        dialogBody.setText(R.string.order_error_5032_body);
                        customDialog.show();
                        break;
                    case 7017:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_7017_title);
                        dialogBody.setText(R.string.order_error_7017_body);
                        customDialog.show();
                        break;
                    default:
                        dialogLeftButton.setText("");
                        dialogRightButton.setText(R.string.alert_close);
                        dialogTitle.setText(R.string.order_error_default_title);
                        dialogBody.setText(R.string.order_error_default_body);
                        customDialog.show();
                }


//                if (resultCode != 200) {
//                    showDialog(getView());
//                } else if (resultCode == 200) {
//                    long response = parserJSON.parseOrdersData(jsonData, getContext());
//                    ArrayList<LatLng> ordersCoord = new ArrayList<>();
//
//
//                    if (response != -1) {
//                        //Loading markers of orders to the map.
////                        setOrderMarker(orders);
//
//
////                        Toast.makeText(getContext(), R.string.toast_orders_success, Toast.LENGTH_SHORT).show();
//                        //We wait until the request is successful and correct to poblate the list
//
//
//                        myDataset = db.GetOrdersAddress();
////                        setOrderMarker(myDataset);
//                        mAdapter = new OrdersAdapter(myDataset, this, getContext());
//                        mLayoutManager = new LinearLayoutManager(getContext());
//                        recyclerView.setLayoutManager(mLayoutManager);
//                        recyclerView.setItemAnimator(new DefaultItemAnimator());
//                        recyclerView.setAdapter(mAdapter);
//                        recyclerView.setVisibility(View.VISIBLE);
//                        progressBar.setVisibility(View.GONE);
//                    } else {
//                        Toast.makeText(getContext(), R.string.toast_orders_error + ", last id:" + response, Toast.LENGTH_LONG).show();
//                    }
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        test.setText(data);

    }

    @Override
    public void onPostSuccess(String serverResponse) {

    }

    @Override
    public void onPostException(String error) {

    }

    public void showDialog(View v) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.setContentView(R.layout.custom_dialog);

        ImageView icon = (ImageView) dialog.findViewById(R.id.icondialog);
        icon.setImageResource(R.mipmap.ic_about);

        TextView title = (TextView) dialog.findViewById(R.id.txvtitledialog);
        title.setTypeface(boldFont);
        title.setText(R.string.dialog_orders_error_title);

        TextView message = (TextView) dialog.findViewById(R.id.txvdialog);
        message.setTypeface(regularFont);
        message.setTextColor(Color.BLACK);
        message.setText(R.string.dialog_orders_error_message);

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.btndialogok);
        dialogButtonOk.setTextColor(Color.BLACK);
        dialogButtonOk.setTypeface(boldFont);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Home2 home2 = new Home2();
                home2.showFragment(1, 0);
            }
        });

        Button dialogButtonDecline = (Button) dialog.findViewById(R.id.btndialogdecline);
        dialogButtonDecline.setTextColor(Color.BLACK);
        dialogButtonDecline.setTypeface(boldFont);

        dialogButtonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.setMinZoomPreference(16f);

        try {
            orderLocations = new ArrayList<>();
            orderLocations = db.GetOrderLocations();

            if (orderLocations.size() >= 1) {
                for (int i = 0; i < orderLocations.size(); i++) {
                    googleMap.addMarker(
                            new MarkerOptions().position(orderLocations.get(i))
                                    .title("Location " + (i + 1))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_gaszen_location_pointer))
                    );

                }
                if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    showPermissionDialog();

                } else {

                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(orderLocations.get(0)));

                }
            } else {

            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), R.string.get_locations_error, Toast.LENGTH_LONG).show();
        }

    }

    private void showPermissionDialog() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);

    }


}
