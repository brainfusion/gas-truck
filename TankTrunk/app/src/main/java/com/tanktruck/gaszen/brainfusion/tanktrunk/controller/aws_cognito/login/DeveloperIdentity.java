package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;

//import mx.brainfusion.gaszenpipas.persistence.GaszenDB_DTO;

/**
 * Created by erickleonel on 04/01/2016.
 */
public class DeveloperIdentity {
    private String openIdToken = null;
    private Context context;
    private GaszenDB_DTO gaszenDB;
    private final String TAG="DeveloperIdentity";

    public DeveloperIdentity(Context context){
        this.context=context;
        gaszenDB = GaszenDB_DTO.getInstance(context);
        openIdToken = AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager
                .getDefaultSharedPreferences(this.context));
    }

    public void getOpenIdToken(boolean expired) {
        try {
            Log.d(TAG, "getOpenIdToken()");
            if (expired) {
                updateOpenIdToken();
            } else {
                if (openIdToken == null) {
                    Log.d(TAG, "Obteniendo nuevo token (TOKEN NULL)...");
                    ((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider
                            .getIdentityProvider()).refresh();
                } else {
                    openIdToken = AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager
                            .getDefaultSharedPreferences(this.context));
                }
                Log.d(TAG, "Nuevo token obtenido: " + openIdToken);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void updateOpenIdToken(){
        try {
            Log.d(TAG,"UPDATE OPEN ID TOKEN()");
            if(CognitoSyncClientManager.credentialsProvider!=null && ((DeveloperAuthenticationProvider) CognitoSyncClientManager.developerIdentityProvider)!=null){
                Log.d("AWS","Credential Provider NOT NULL");
                ((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider.getIdentityProvider()).refresh();
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }catch (Exception e){
         e.printStackTrace();
        }
    }
}