package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

/**
 * Created by carlosrodriguez on 3/23/17.
 */

public class Tank {
    private Integer id_tank;
    private Integer tank_actual_level;
    private Integer tank_capacity;

    /*public Tank (Integer actual_Level, String Capacity){

    }*/

    public Integer getTank_actual_level() {
        return tank_actual_level;
    }

    public void setTank_actual_level(Integer tank_actual_level) {
        this.tank_actual_level = tank_actual_level;
    }

    public Integer getTank_capacity() {
        return tank_capacity;
    }

    public void setTank_capacity(Integer tank_capacity) {
        this.tank_capacity = tank_capacity;
    }

    public Integer getId_tank() {
        return id_tank;
    }

    public void setId_tank(Integer id_tank) {
        this.id_tank = id_tank;
    }
}
