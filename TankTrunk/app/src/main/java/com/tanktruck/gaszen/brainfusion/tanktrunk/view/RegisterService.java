package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlosrodriguez on 1/19/17.
 */

public class RegisterService extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText edtSearch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View regService = inflater.inflate(R.layout.fragment_register_service_1, container, false);


        List<Client> myDataset = new ArrayList<>();
        EditText editTextSearch = (EditText) regService.findViewById(R.id.edtsearch);

        TextInputLayout tilSearch = (TextInputLayout) regService.findViewById(R.id.text_input_layout_search);
        edtSearch = (EditText) regService.findViewById(R.id.edt_regusr_name);
        tilSearch.setHint(getString(R.string.service_search));

        /*recyclerView = (RecyclerView) regservice.findViewById(R.id.recycler_users);

        mAdapter = new ServicesAdapter(myDataset, regservice);
        mLayoutManager = new LinearLayoutManager(regservice.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/



        return regService;
    }
}
