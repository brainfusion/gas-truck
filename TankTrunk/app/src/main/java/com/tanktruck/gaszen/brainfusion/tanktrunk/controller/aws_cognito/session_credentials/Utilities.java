/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.session_credentials;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Utility class for communicating with sample Cognito developer authentication.
 */
public class Utilities {
    private static final String TAG="Utilities";

    public static String getToken(String json){//Function for new JSON Response and change of frame Input Login function
        try {
            JSONObject jsonObject=new JSONObject(json).getJSONObject("result_contents");
            Log.d(TAG,"TOKEN= "+jsonObject.getString("Token"));
            return jsonObject.getString("Token");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getIdentityId(String json){//Function for new JSON Response and change of frame Input Login function
        try {
            JSONObject jsonObject=new JSONObject(json).getJSONObject("result_contents");
            Log.d(TAG,"IdentityId= "+jsonObject.getString("IdentityId"));
            return jsonObject.getString("IdentityId");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String extractElementToken(String json, String element) {
        try {
            boolean hasElement = (json.indexOf(element) != -1);
            if (hasElement) {

                int elementIndex = json.indexOf(element + element.length());
                int startIndex = json.indexOf("=", elementIndex);
                int endIndex = json.indexOf(",", startIndex + 1);
                return json.substring(startIndex + 1, endIndex);
            }
        }catch (Exception e){
            Log.d("Utilities", "Exception: "+e.toString());
            e.printStackTrace();
        }
        return null;
    }

    public static String extractElementIdenID(String json, String element) {
        try {
            boolean hasElement = (json.indexOf(element) != -1);
            if (hasElement) {
                int elementIndex = json.indexOf(element) + element.length();
                int startIndex = elementIndex;
                int endIndex = json.length() - 1;
                return json.substring(startIndex + 1, endIndex);
            }
        }catch (Exception e){
            Log.d("Utilities", "Exception: "+e.toString());
            e.printStackTrace();
        }
        return null;
    }
}
