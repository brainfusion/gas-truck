package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.management;

import android.content.Context;
import android.util.Log;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.aws_lambda.LambdaManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.CognitoSyncClientManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.DeveloperAuthenticationProvider;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.DeveloperIdentity;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;

/**
 * Created by erickleonel on 29/12/2015.
 */
public class GaszenManager {
    private LambdaManager lambdaManager = null;
    private Context context;
    private DeveloperIdentity developerIdentity;

    public GaszenManager(Context context) {
        this.context = context;
    }

    public void initManager() {
        Log.d("AWS", "initManager");
        developerIdentity = new DeveloperIdentity(context);
        developerIdentity.getOpenIdToken(false);
    }

    public void GetOrdersGM(final String data, short id, final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.GetOrders(data, id,cloudSyncCallback);
    }

    public void UpdateOrderStatusGM(final String data, short id, final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.UpdateOrderStatus(data, id,cloudSyncCallback);
    }

    public void RegisterClient(final String data, short id, final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.RegisterClient(data, id,cloudSyncCallback);
    }

    public void RegisterService(final String data, short id, final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.RegisterService(data, id,cloudSyncCallback);
    }

    /**
     * A function to add a new gauge in the Internet Server and Local Storage
     *
     * @param data
     */
    /*public void addNewGauge(final String data, short id,final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.sendNewDevice(data, id, cloudSyncCallback);
        //1.- Call Lambda function

        //2.- Check success
        //3.- Call Local Storage
    }

    public void deleteGauge(final String data, short id, final CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.disableDevice(data, id,cloudSyncCallback);
    }

    public void getGaugeReading(String data, short id, CloudSyncCallback cloudSyncCallback) {
        try {
            lambdaManager = new LambdaManager(context);
            lambdaManager.getLastReading(data, id, cloudSyncCallback);
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    /**
     * A function to get user login
     *
     * @param email
     * @param password
     * @param cloudSyncCallback
     */
    public void initializeUserSession(String email, String password, CloudSyncCallback cloudSyncCallback, short typeSession) {
//        CognitoSyncClientManager.credentialsProvider
//                .clearCredentials();
        ((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider
                .getIdentityProvider()).login(email, password, cloudSyncCallback, context, typeSession);
    }

//    public void updateGaugeDaysLeft(String data, CloudSyncCallback cloudSyncCallback) {//Deprecated
//        lambdaManager = new LambdaManager(context);
//        lambdaManager.getDaysLeft(data, cloudSyncCallback);
//    }

//    public void updateRechargeTable(String data, CloudSyncCallback cloudSyncCallback) {//Unused
//        lambdaManager = new LambdaManager(context);
//        lambdaManager.getRefillTableData(data, cloudSyncCallback);
//    }

    /*public void validateDeviceAndPin(String data,short id, CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.deviceValidator(data,id,cloudSyncCallback);
    }

    public void resetPassword(String data,short id, CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.resetCurrentPassword(data, id, cloudSyncCallback);
    }

    public void restoreUserSession( String data, short id,CloudSyncCallback cloudSyncCallback) {
        lambdaManager = new LambdaManager(context);
        lambdaManager.restoreUserDevices(data, id,cloudSyncCallback);
    }
    public void addDeviceAddress( String data,short id,CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.setDeviceLocationRegistration(data,id,cloudSyncCallback);
    }

    public void setOrder(String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.placeAnOrder(data,id, cloudSyncCallback);
    }

    public void getGasSuppliersByCode(String data, short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.getListProvidersByZip(data, id, cloudSyncCallback);
    }

    public void getGasSupplier(String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.getProvider(data, id,cloudSyncCallback);
    }

    public void synchronizeListDevices(String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.getListDevices(data,id,cloudSyncCallback);
    }
    public void setDeviceToken( String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.setPushToken(data, id,cloudSyncCallback);
    }

    public void sendAppFeedback(String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.setAppFeedback(data, id,cloudSyncCallback);
    }
    public void sendSupplierFeedback(String data,short id, CloudSyncCallback cloudSyncCallback){
        lambdaManager = new LambdaManager(context);
        lambdaManager.setSupplierFeedback(data, id,cloudSyncCallback);
    }

    public void removeGCMToken(String data, short id){
        lambdaManager = new LambdaManager(context);
        lambdaManager.deletePushToken(data, id);
    }*/
}
