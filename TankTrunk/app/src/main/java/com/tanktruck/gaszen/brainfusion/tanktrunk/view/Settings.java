package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;

/**
 * Created by thomasepp on 2017-01-06.
 */

public class Settings extends android.support.v4.app.Fragment {

    private Typeface regularFont = null, boldFont = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");

        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        return v;
    }
}
