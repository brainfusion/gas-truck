/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * <p>
 * http://aws.amazon.com/apache2.0
 * <p>
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;

import android.content.Context;
import android.util.Log;

import java.net.URL;

/**
 * This class is used to communicate with the sample Cognito developer
 * authentication application sample.
 */
public class AmazonCognitoSampleDeveloperAuthenticationClient {
    private static final String LOG_TAG = "AmazonCognitoSampleDeveloperAuthenticationClient";

    /**
     * The endpoint for the sample Cognito developer authentication application.
     */
    private final URL endpoint;

    /**
     * The appName declared by the sample Cognito developer authentication
     * application.
     */
    private final String appName;
    private Context context;

    /**
     * The shared preferences where user key is stored.
     */
    //private final SharedPreferences sharedPreferences;

    //public AmazonCognitoSampleDeveloperAuthenticationClient(SharedPreferences sharedPreferences, URL endpoint, String appName) {
    public AmazonCognitoSampleDeveloperAuthenticationClient(Context context, URL endpoint, String appName) {
        this.endpoint = endpoint;
        this.appName = appName.toLowerCase();
        this.context=context;
        // this.sharedPreferences = sharedPreferences;
    }
    /**
     * Using the given username and password, securily communictes the Key for
     * the user's account.
     */
    public Response login(String username, String password, short typeSession) {
        Response response = Response.SUCCESSFUL;
        //if (AmazonSharedPreferencesWrapper.getUidForDevice(this.sharedPreferences) == null) {
        // String uid = AmazonCognitoSampleDeveloperAuthenticationClient.generateRandomString();
        String uid = "";
        LoginRequest loginRequest = new LoginRequest(this.endpoint, this.appName, uid, username, password);
        //ResponseHandler handler = new LoginResponseHandler(loginRequest.getDecryptionKey());
        ResponseHandler handler = new LoginResponseHandler();

        response = this.processRequest(loginRequest, handler,typeSession);
        if (response.requestWasSuccessful()) {
        }
        return response;
    }

    /**
     * Process Request
     */
    protected Response processRequest(Request request, ResponseHandler handler, short typeSession) {
        Response response = null;
        int retries = 2;
        do {
            response = CognitoSampleDeveloperAuthenticationService.sendRequest(context,request, handler, typeSession);
            if (response.requestWasSuccessful()) {
                return response;
            } else {
                Log.d("AWS",
                        "Request to Cognito Sample Developer Authentication Application failed with Code: ["
                                + response.getResponseCode() + "] Message: ["
                                + response.getResponseMessage() + "]");
            }
        } while (retries-- > 0);
        return response;
    }
}
