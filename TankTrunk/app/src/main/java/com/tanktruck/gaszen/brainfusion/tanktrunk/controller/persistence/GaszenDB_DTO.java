package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Client;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Order;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.OrderAddress;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.OrderData;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.SessionDAO;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Tank;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.UserDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//import mx.brainfusion.gaszenpipas.controller.aws_cognito.login.AmazonSharedPreferencesWrapper;

//This is the class that handles every sqlite action.

/**
 * Created by erickleonel on 11/12/2015.
 */
public class GaszenDB_DTO extends SQLiteOpenHelper {
    private static GaszenDB_DTO gaszenDBInstance = null;
    private static final String DATABASE_NAME = "gaszenPipasDB";
    private static final int DATABASE_VERSION = 3;  //lasted 1 //2
    private AtomicInteger mOpenCounter = new AtomicInteger();
    private SQLiteDatabase mDB;
    private Context mContext;
    private final String TAG = "GaszenDB_DTO";

    private GaszenDB_DTO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public static synchronized GaszenDB_DTO getInstance(Context context) {
        if (gaszenDBInstance == null) {
            gaszenDBInstance = new GaszenDB_DTO(context);
        }
        return gaszenDBInstance;
    }

    private synchronized SQLiteDatabase openDatabase() {//on Update
        try {
            if (mOpenCounter.incrementAndGet() == 1) {
                // Opening new database
                mDB = gaszenDBInstance.getWritableDatabase();
            }
        } catch (SQLiteCantOpenDatabaseException e) {
            Log.e(TAG, "SQLiteCantOpenDatabaseException: " + e.toString());
            gaszenDBInstance = getInstance(mContext);

            e.printStackTrace();
        } catch (SQLiteException e) {
            Log.e(TAG, "SQLiteException: " + e.toString());
            //onCreate(mDB);
            gaszenDBInstance = getInstance(mContext);
            e.printStackTrace();
        }
        return mDB;
    }

    private synchronized void closeDatabase() {
        if (mOpenCounter.decrementAndGet() == 0) {
            mDB.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //        /*USER Table*/
        db.execSQL("CREATE TABLE user (id_user INTEGER PRIMARY KEY AUTOINCREMENT, name_user TEXT UNIQUE NOT NULL, email TEXT UNIQUE NOT NULL, pass TEXT NOT NULL)");

        /*SESSION Table*/
        db.execSQL("CREATE TABLE session (id_session INTEGER PRIMARY KEY AUTOINCREMENT, id_user INTEGER NOT NULL, state_session INTEGER NOT NULL, type INTEGER NOT NULL," +
                "FOREIGN KEY(id_user) REFERENCES user(id_user))");

        /*DEVICE Table
        db.execSQL("CREATE TABLE device (id_device INTEGER PRIMARY KEY, state_device INTEGER NOT NULL, serial_number TEXT NOT NULL, data_register INTEGER NOT NULL, id_address INTEGER," +
                "FOREIGN KEY(id_address) REFERENCES address(id_address))");*/

        /*DEVICE DATA Table
        db.execSQL("CREATE TABLE device_data (id_data INTEGER PRIMARY KEY AUTOINCREMENT, id_user INTEGER NOT NULL, id_device INTEGER NOT NULL, id_data_definition INTEGER NOT NULL, timestamp INTEGER, data_value TEXT NOT NULL," +
                "FOREIGN KEY(id_user) REFERENCES user(id_user), FOREIGN KEY(id_device) REFERENCES device(id_device))");//, FOREIGN KEY(id_data_definition) REFERENCES device_data_definition(id_definition))");*/

        /*CHARTS Table
        db.execSQL("CREATE TABLE charts (id_value INTEGER PRIMARY KEY AUTOINCREMENT, id_device INTEGER NOT NULL, chart_type INTEGER NOT NULL, period INTEGER NOT NULL, timestamp INTEGER NOT NULL, value INTEGER NOT NULL, percentage INTEGER," +
                "FOREIGN KEY(id_device) REFERENCES device(id_device))");*/

        /*ADDRESS Table  DEPRECATED
        db.execSQL("CREATE TABLE address (id_address INTEGER PRIMARY KEY AUTOINCREMENT, street_name TEXT NOT NULL, ext_address_number TEXT NOT NULL, int_address_number TEXT, neighborhood TEXT, zip_postal_code INT NOT NULL, locality TEXT," +
                "city TEXT, state TEXT, country TEXT, reference TEXT, tank_location TEXT, latitude TEXT, longitude TEXT ,address_type INT NOT NULL)");*/

        //CLIENT Table
        db.execSQL("CREATE TABLE client (id_client INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "client_name TEXT NOT NULL, client_street_address TEXT NOT NULL, " +
                "client_neighborhood_address TEXT NOT NULL, client_city TEXT NOT NULL, " +
                "client_ext_number INT NOT NULL, client_int_number INT NOT NULL," +
                "client_postal_code INT NOT NULL," +
                "client_lat INT NOT NULL, client_lng INT NOT NULL)");

        //TANK Table
        db.execSQL("CREATE TABLE tank (id_tank INTEGER PRIMARY KEY AUTOINCREMENT, tank_actual_level INT NOT NULL, tank_capacity INT NOT NULL)");

        //ORDER Table
        //db.execSQL("CREATE TABLE orders (id_order INTEGER PRIMARY KEY AUTOINCREMENT, qty_liters TEXT NOT NULL, payment_method TEXT NOT NULL, sale_status TEXT NOT NULL, price_per_liter TEXT NOT NULL, currency TEXT NOT NULL, total_sale TEXT NOT NULL, timestamp TEXT NOT NULL, id_client INT NOT NULL, id_tank INT NOT NULL)");
        db.execSQL("CREATE TABLE orders (id_order INTEGER PRIMARY KEY AUTOINCREMENT, qty_liters TEXT NOT NULL, payment_method TEXT NOT NULL, sale_status TEXT NOT NULL, price_per_liter TEXT NOT NULL, currency TEXT NOT NULL, total_sale TEXT NOT NULL, timestamp TEXT NOT NULL, device_uuid TEXT NOT NULL, order_id TEXT NOT NULL, payment_method_authorization TEXT NOT NULL, sale_payment_status TEXT NOT NULL, email TEXT NOT NULL, payment_method_id TEXT NOT NULL, id_client INT NOT NULL, id_tank INT NOT NULL, FOREIGN KEY(id_client) REFERENCES client(id_client), FOREIGN KEY(id_tank) REFERENCES tank(id_tank))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "onUpgrade: oldVersion=" + oldVersion + " newVersion=" + newVersion);
        Log.d(TAG, "Old vesion: " + oldVersion);
        switch (oldVersion) {
            case 1:
                db.execSQL("DROP TABLE IF EXISTS orders");
                db.execSQL("CREATE TABLE orders (id_order INTEGER PRIMARY KEY AUTOINCREMENT, qty_liters TEXT NOT NULL, payment_method TEXT NOT NULL, sale_status TEXT NOT NULL, price_per_liter TEXT NOT NULL, currency TEXT NOT NULL, total_sale TEXT NOT NULL, timestamp TEXT NOT NULL, device_uuid TEXT NOT NULL, order_id TEXT NOT NULL, payment_method_authorization TEXT NOT NULL, sale_payment_status TEXT NOT NULL, email TEXT NOT NULL, payment_method_id TEXT NOT NULL, id_client INT NOT NULL, id_tank INT NOT NULL, FOREIGN KEY(id_client) REFERENCES client(id_client), FOREIGN KEY(id_tank) REFERENCES tank(id_tank))");
                break;
            case 2:
                db.execSQL("DROP TABLE IF EXISTS client");
                db.execSQL("CREATE TABLE client (id_client INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "client_name TEXT NOT NULL, client_street_address TEXT NOT NULL, " +
                        "client_neighborhood_address TEXT NOT NULL, client_city TEXT NOT NULL, " +
                        "client_ext_number INT NOT NULL, client_int_number INT NOT NULL," +
                        "client_postal_code INT NOT NULL," +
                        "client_lat INT NOT NULL, client_lng INT NOT NULL)");
                break;
            default:
                break;
        }
    }
    /* Data Base Functions Stuff */

    public UserDAO getUser(int idUser) throws SQLException, Exception { //(Comentado de momento una lineas CHECAR !!!!!!!!!!!!!!!!!!)
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();
        UserDAO user = null;
//        Statement stm=null;
//        ResultSet rs;

        Cursor cursor = null;
        //cursor = db.rawQuery("SELECT id_user, name_user, email, pass FROM user WHERE id_user=" + idUser, null);
        cursor = db.rawQuery("SELECT * FROM user WHERE id_user=" + idUser, null);
        if (cursor.moveToFirst()) {
            do {
                user = new UserDAO();
                user.addUser(cursor.getInt(0), //User Id
                        cursor.getString(1),//User name
                        cursor.getString(2),//User email
                        cursor.getString(3),//User password
                        cursor.getString(4),//User Phone number
                        cursor.getShort(5));//Term and Conditions
                //user.addUser();
//-                user.setIdUser(cursor.getInt(0));
//-                user.setNameUser(cursor.getString(1));
//-                user.setEmail(cursor.getString(2));
                // user.setPassword(cursor.getString(3));

            } while (cursor.moveToNext());
        }


//        rs=stm.executeQuery("SELECT id_user, name_user, email, password FROM user WHERE id_user="+idUser);
//
//        while(rs.next()){
//            user=new UserDAO();
//            user.setIdUser(rs.getInt("id_user"));
//            user.setNameUser(rs.getString("name_user"));
//            user.setEmail(rs.getString("email"));
//            user.setPassword(rs.getString("password"));
//        }
//        cursor.close();
//        db.close();
        closeDatabase();
        return user;
    }

    public SessionDAO getSession() throws SQLException, Exception {
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();
        SessionDAO session = null;

        Cursor cursor = null;
        //--cursor = db.rawQuery("SELECT id_session, id_user, state_session, percentage_alert, alert_type FROM session WHERE state_session=" + 1, null);
        //cursor = db.rawQuery("SELECT id_session, id_user, state_session FROM session WHERE state_session=" + 1, null);
        cursor = db.rawQuery("SELECT * FROM session WHERE state_session=1", null);
        if (cursor.moveToFirst()) {
            do {
                session = new SessionDAO();
                session.addSession(cursor.getInt(0), cursor.getInt(1), cursor.getShort(2), cursor.getShort(3));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        return session;
    }

    public SessionDAO getSession(int id) throws SQLException, Exception {
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();
        SessionDAO session = null;
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM session WHERE id_user=" + id, null);
        if (cursor.moveToFirst()) {
            do {
                session = new SessionDAO();
                session.addSession(cursor.getInt(0), cursor.getInt(1), cursor.getShort(2), cursor.getShort(3));
//                session.setIdSession(cursor.getInt(0));
//                session.setIdUser(cursor.getInt(1));
//                session.setStateSession(cursor.getInt(2));
                //session.setPercentageAlert(cursor.getInt(3));
                //session.setAlertType(cursor.getInt(4));

            } while (cursor.moveToNext());
        }
//        cursor.close();
//        db.close();
        closeDatabase();
        return session;
    }

    public int updateSession(boolean flag) throws SQLException, Exception {
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        ContentValues contentValues = new ContentValues();
        Integer res = 0;

        if (flag) {
            contentValues.put("state_session", 0);
            res = db.update("session", contentValues, "state_session=1", null);
        } else if (!flag) {
            contentValues.put("state_session", 1);
            res = db.update("session", contentValues, "state_session=0", null);
        }

        closeDatabase();

        return res;
    }

    public int swipeUserSession(String email) {
        try {
            gaszenDBInstance = getInstance(mContext);
            //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
            SQLiteDatabase db = gaszenDBInstance.openDatabase();

            String confEmail = "";
            Integer res = 0;

            Cursor cursor = null;
            cursor = db.rawQuery("SELECT email FROM user WHERE email='" + email + "'", null);
            if (cursor.moveToFirst()) {
                confEmail = cursor.getString(0);
            }

            if (confEmail.contentEquals(email)) {
                res = updateSession(false);
            } else {
                db.delete("session", null, null);
                db.delete("user", null, null);
            }

            closeDatabase();
            return res;
        } catch (Exception ex) {
            Log.e("Error: ", ex.toString());
            return 0;
        }
    }

    public long saveUserSession(String name, String email, String pass) {
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        ContentValues contentValuesUser = new ContentValues();
        contentValuesUser.put("name_user", name);
        contentValuesUser.put("email", email);
        contentValuesUser.put("pass", pass);

        long res = db.insert("user", null, contentValuesUser);

        if (res != -1) {
            ContentValues contentValuesState = new ContentValues();
            contentValuesState.put("id_user", res);
            contentValuesState.put("state_session", 1);
            contentValuesState.put("type", 0);

            res = db.insert("session", null, contentValuesState);
        }

        closeDatabase();

        return res;
    }

    public long saveOrdersData(Client client, Tank tank, Order order) {
        gaszenDBInstance = getInstance(mContext);
        //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        ContentValues contentValuesUser = new ContentValues();
        contentValuesUser.put("client_name", client.getClient_name());
        contentValuesUser.put("client_street_address", client.getClient_street_address());
        contentValuesUser.put("client_neighborhood_address", client.getClient_neighborhood_address());
        contentValuesUser.put("client_city", client.getClient_city());
        contentValuesUser.put("client_ext_number", client.getClient_ext_number());
        contentValuesUser.put("client_int_number", client.getClient_int_number());
        contentValuesUser.put("client_postal_code", client.getClient_postal_code());
        contentValuesUser.put("client_lat", client.getClient_lat());
        contentValuesUser.put("client_lng", client.getClient_lng());

        long idClient = db.insert("client", null, contentValuesUser);
        long idTank = 0;
        long idOrder = 0;

        if (idClient != -1) {
            ContentValues contentValuesTank = new ContentValues();
            contentValuesTank.put("tank_actual_level", tank.getTank_actual_level());
            contentValuesTank.put("tank_capacity", tank.getTank_capacity());

            idTank = db.insert("tank", null, contentValuesTank);
        } else {
            return 0;
        }


        if (idTank != -1) {
            ContentValues contentValuesOrder = new ContentValues();
            contentValuesOrder.put("qty_liters", order.getQty_liters());
            contentValuesOrder.put("payment_method", order.getPayment_method());
            contentValuesOrder.put("sale_status", order.getSale_status());
            contentValuesOrder.put("price_per_liter", order.getPrice_per_liter());
            contentValuesOrder.put("currency", order.getCurrency());
            contentValuesOrder.put("total_sale", order.getTotal_sale());
            contentValuesOrder.put("timestamp", order.getTimestamp());
            contentValuesOrder.put("id_client", idClient);
            contentValuesOrder.put("id_tank", idTank);
            contentValuesOrder.put("device_uuid", order.getDevice_uuid());
            contentValuesOrder.put("order_id", order.getOrder_id());
            contentValuesOrder.put("payment_method_authorization", order.getPayment_method_authorization());
            contentValuesOrder.put("sale_payment_status", order.getSale_payment_status());
            contentValuesOrder.put("email", order.getEmail());
            contentValuesOrder.put("payment_method_id", order.getPayment_method_id());

            idOrder = db.insert("orders", null, contentValuesOrder);
        } else {
            return 0;
        }

        closeDatabase();

        return idOrder;
    }

    public int ClearOrders() {
        try {
            gaszenDBInstance = getInstance(mContext);
            //SQLiteDatabase db = gaszenDBInstance.getWritableDatabase();
            SQLiteDatabase db = gaszenDBInstance.openDatabase();

            db.delete("orders", null, null);
            db.delete("client", null, null);
            db.delete("tank", null, null);

            return 1;
        } catch (Exception ex) {
            Log.e("Error: ", ex.toString());
            return 0;
        }
    }

    public List<OrderAddress> GetOrdersAddress() {
        List<OrderAddress> orders = new ArrayList<>();
        gaszenDBInstance = getInstance(mContext);
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        OrderAddress orderAddress = null;
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM orders", null);
        if (cursor.moveToFirst()) {
            do {
                orderAddress = new OrderAddress();
                orderAddress.setId_order(cursor.getInt(0));
                orderAddress.setId_client(cursor.getInt(14));

                List<String> data = GetUserAddress(orderAddress.getId_client());

                orderAddress.setClient_street_address(data.get(0));
                orderAddress.setClient_neighborhood_address(data.get(1));

                orders.add(orderAddress);
            } while (cursor.moveToNext());
        }

        closeDatabase();
        return orders;
    }

    public List<String> GetUserAddress(Integer id_client) {
        List<String> data = new ArrayList<>();
        gaszenDBInstance = getInstance(mContext);
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT client_street_address, client_neighborhood_address  FROM client WHERE id_client=" + id_client, null);
        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(0));
                data.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();

        closeDatabase();
        return data;
    }

    public OrderData GetOrderInfo(Integer id_order) {
        OrderData data = new OrderData();
        gaszenDBInstance = getInstance(mContext);
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT qty_liters, payment_method, price_per_liter, currency, total_sale, id_client, id_tank FROM orders WHERE id_order=" + id_order, null);
        if (cursor.moveToFirst()) {
            do {
                Order order = new Order();
                order.setQty_liters(cursor.getString(0));
                order.setPayment_method(cursor.getString(1));
                order.setPrice_per_liter(cursor.getString(2));
                order.setCurrency(cursor.getString(3));
                order.setTotal_sale(cursor.getString(4));
                order.setId_client(cursor.getInt(5));
                order.setId_tank(cursor.getInt(6));
                data.setOrder(order);
            } while (cursor.moveToNext());
        }

        if (data.getOrder() != null) {
            cursor = null;
            cursor = db.rawQuery("SELECT * FROM client WHERE id_client=" + data.getOrder().getId_client(), null);
            if (cursor.moveToFirst()) {
                do {
                    Client client = new Client();
                    client.setClient_name(cursor.getString(1));
                    client.setClient_street_address(cursor.getString(2));
                    client.setClient_neighborhood_address(cursor.getString(3));
                    client.setClient_city(cursor.getString(4));
                    client.setClient_ext_number(cursor.getInt(5));
                    client.setClient_int_number(cursor.getInt(6));
                    client.setClient_postal_code(cursor.getInt(7));
                    client.setClient_lat(cursor.getDouble(8));
                    client.setClient_lng(cursor.getDouble(9));
                    data.setClient(client);
                } while (cursor.moveToNext());
            }
        }

        if (data.getOrder() != null) {
            cursor = null;
            cursor = db.rawQuery("SELECT * FROM tank WHERE id_tank=" + data.getOrder().getId_tank(), null);
            if (cursor.moveToFirst()) {
                do {
                    Tank tank = new Tank();
                    tank.setTank_actual_level(cursor.getInt(1));
                    tank.setTank_capacity(cursor.getInt(2));
                    data.setTank(tank);
                } while (cursor.moveToNext());
            }
        }

        closeDatabase();
        return data;
    }

    public List<String> GetStatusInfo(Integer id_order) {
        List<String> data = new ArrayList<>();
        gaszenDBInstance = getInstance(mContext);
        SQLiteDatabase db = gaszenDBInstance.openDatabase();

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT device_uuid, timestamp, sale_status  FROM orders WHERE id_order=" + id_order, null);
        if (cursor.moveToFirst()) {
            do {
                data.add(cursor.getString(0));
                data.add(cursor.getString(1));
                data.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        closeDatabase();
        return data;
    }

    public List<LatLng> GetOrderLocations() {
        List<LatLng> orderLocations = new ArrayList<>();
        gaszenDBInstance = getInstance(mContext);
        SQLiteDatabase db = gaszenDBInstance.openDatabase();
        Double Lat;
        Double Lng;

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT client_lat, client_lng FROM client", null);

        if (cursor.moveToFirst()) {
            do {
                Lat = cursor.getDouble(0);
                Lng = cursor.getDouble(1);

                LatLng location = new LatLng(Lat, Lng);
                orderLocations.add(location);

            } while (cursor.moveToNext());
        }

        closeDatabase();

        return orderLocations;

    }
}
