package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.*;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.tanktruck.gaszen.brainfusion.tanktrunk.Manifest;
import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.AmazonSharedPreferencesWrapper;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;

public class Home2 extends AppCompatActivity implements CloudSyncCallback {

    private Typeface boldFont = null, regularFont = null;
    private Toolbar toolbar;
    public static boolean openPreferences = false;
    private final String TAG = "Home";
    private static int currentFragment = 1;
    private GaszenDB_DTO gaszenDB = null;
    private int MY_PERMISSIONS_REQUEST_LOCATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);

        boldFont = Typeface.createFromAsset(this.getAssets(), "Nexa-Bold.otf");
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(this.getAssets(), "LatoRegular.ttf");

//        TextView txvRegClient = (TextView) findViewById(R.id.txv_menu_register_client);
//        txvRegClient.setTypeface(regularFont);
        TextView txvOrders = (TextView) findViewById(R.id.txv_menu_orders);
        txvOrders.setTypeface(regularFont);
//        TextView txvRegService = (TextView) findViewById(R.id.txv_menu_register_service);
//        txvRegService.setTypeface(regularFont);
        TextView txvAlerts = (TextView) findViewById(R.id.txv_menu_alerts);
        txvAlerts.setTypeface(regularFont);
        TextView txvStatus = (TextView) findViewById(R.id.txv_menu_status);
        txvStatus.setTypeface(regularFont);
        TextView txvAbout = (TextView) findViewById(R.id.txv_menu_about);
        txvAbout.setTypeface(regularFont);

        ImageButton btnLogout = (ImageButton) findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(v);
                currentFragment = 1;
            }
        });


        final TableRow tblOrders = (TableRow) findViewById(R.id.tbl_orders);
//        final TableRow tblAddClient = (TableRow) findViewById(R.id.tbl_add_client);
//        final TableRow tblAddService = (TableRow) findViewById(R.id.tbl_add_service);
        final TableRow tblAlerts = (TableRow) findViewById(R.id.tbl_alerts);
        final TableRow tblStatus = (TableRow) findViewById(R.id.tbl_status);
        final TableRow tblAbout = (TableRow) findViewById(R.id.tbl_about);

        tblOrders.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.button_background_active);
//                tblAddClient.setBackgroundResource(R.drawable.button_background_idle);
//                tblAddService.setBackgroundResource(R.drawable.button_background_idle);
                tblAlerts.setBackgroundResource(R.drawable.button_background_idle);
                tblStatus.setBackgroundResource(R.drawable.button_background_idle);
                tblAbout.setBackgroundResource(R.drawable.button_background_idle);
                if (currentFragment != 1) {
                    showFragment(1, null);
                }

            }
        });

        tblOrders.setBackgroundResource(R.drawable.button_background_active);

//        tblAddClient.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                v.setBackgroundResource(R.drawable.button_background_active);
//                tblOrders.setBackgroundResource(R.drawable.button_background_idle);
//                tblAddService.setBackgroundResource(R.drawable.button_background_idle);
//                tblAlerts.setBackgroundResource(R.drawable.button_background_idle);
//                tblStatus.setBackgroundResource(R.drawable.button_background_idle);
//                tblAbout.setBackgroundResource(R.drawable.button_background_idle);
//                if (currentFragment != 2) {
//                    showFragment(2, null);
//                }
//            }
//        });
//
//        tblAddService.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                v.setBackgroundResource(R.drawable.button_background_active);
//                tblAddClient.setBackgroundResource(R.drawable.button_background_idle);
//                tblOrders.setBackgroundResource(R.drawable.button_background_idle);
//                tblAlerts.setBackgroundResource(R.drawable.button_background_idle);
//                tblStatus.setBackgroundResource(R.drawable.button_background_idle);
//                tblAbout.setBackgroundResource(R.drawable.button_background_idle);
//                if (currentFragment != 3) {
//                    showFragment(3, null);
//                }
//            }
//        });

        tblStatus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.button_background_active);
//                tblAddClient.setBackgroundResource(R.drawable.button_background_idle);
//                tblAddService.setBackgroundResource(R.drawable.button_background_idle);
                tblAlerts.setBackgroundResource(R.drawable.button_background_idle);
                tblOrders.setBackgroundResource(R.drawable.button_background_idle);
                tblAbout.setBackgroundResource(R.drawable.button_background_idle);
                if (currentFragment != 4) {
                    showFragment(4, null);
                }
            }
        });

        tblAlerts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.button_background_active);
//                tblAddClient.setBackgroundResource(R.drawable.button_background_idle);
//                tblAddService.setBackgroundResource(R.drawable.button_background_idle);
                tblOrders.setBackgroundResource(R.drawable.button_background_idle);
                tblStatus.setBackgroundResource(R.drawable.button_background_idle);
                tblAbout.setBackgroundResource(R.drawable.button_background_idle);
                if (currentFragment != 5) {
                    showFragment(5, null);
                }
            }
        });

        tblAbout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.button_background_active);
//                tblAddClient.setBackgroundResource(R.drawable.button_background_idle);
//                tblAddService.setBackgroundResource(R.drawable.button_background_idle);
                tblOrders.setBackgroundResource(R.drawable.button_background_idle);
                tblStatus.setBackgroundResource(R.drawable.button_background_idle);
                tblAlerts.setBackgroundResource(R.drawable.button_background_idle);
                if (currentFragment != 6) {
                    showFragment(6, null);
                }
            }
        });

        //Initializing toolbar and setting is as actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        //Set the starting fragment
        showFragment(currentFragment, null);

//        setNavItemCount(R.id.tbl_add_client, 90);


        //Check location permission
        if (!checkPermission(this)) {
            showPermissionDialog();
        }
    }

    public void showFragment(int position, Integer idorder) {
        try {
            // Update the main content by replacing fragments
            Fragment fragment = null;
            String tag = null;
            switch (position) {
                case 1:
                    fragment = new Orders();
                    tag = "orders_tag";
                    currentFragment = 1;
                    break;
                case 2:
                    fragment = new RegisterUser();
                    tag = "register_user_tag";
                    currentFragment = 2;
                    break;
                case 3:
                    fragment = new RegisterService();
                    tag = "register_service_tag";
                    currentFragment = 3;
                    break;
                case 4:
                    fragment = new DeviceStatus();
                    tag = "device_status_tag";
                    currentFragment = 4;
                    break;
                case 5:
                    fragment = new Alerts();
                    tag = "alerts_tag";
                    currentFragment = 5;
                    break;
                case 6:
                    fragment = new About();
                    tag = "about_tag";
                    currentFragment = 6;
                    break;
                case 7:
                    fragment = new OrderInfo();
                    Bundle bundle = new Bundle();
                    bundle.putInt("order", idorder);
                    fragment.setArguments(bundle);
                    tag = "oder_info_tag";
                    currentFragment = 7;
                    break;
                default:
                    position = 1;
                    break;
            }
            if (fragment != null) {
                if (position != 1) {
                    openPreferences = true;
                }
                invalidateOptionsMenu();
                replaceFragmentWithAnimation(fragment, tag);

            } else {
                Log.e("Error  ", "Mostrar Fragment" + position);
            }
        } catch (Exception e) {
            Log.e(TAG, "showFragment" + e.toString());
        }
    }

    public void setNavItemCount(@IdRes int itemId, int count) {
        TextView alerts = (TextView) findViewById(R.id.txv_alerts_orders);
        switch (itemId) {
            case R.id.tbl_orders:
                alerts = (TextView) findViewById(R.id.txv_alerts_orders);
                break;
//            case R.id.tbl_add_client:
//                alerts = (TextView) findViewById(R.id.txv_alerts_add_client);
//                break;
        }
        alerts.setVisibility(View.VISIBLE);
        alerts.setTypeface(boldFont);
        int sum = alerts.getText() != "" ? Integer.parseInt(alerts.getText().toString()) : 0;

        sum = sum + count;
        if (sum > 99) {
            alerts.setTextSize(10);
        } else {
            alerts.setTextSize(14);
        }
        alerts.setText(sum > 0 ? String.valueOf(sum) : null);
        //float size = view.getTextSize();
    }

    public void showDialog(View v) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.setContentView(R.layout.custom_dialog);

        ImageView icon = (ImageView) dialog.findViewById(R.id.icondialog);
        icon.setImageResource(R.mipmap.ic_about);

        TextView title = (TextView) dialog.findViewById(R.id.txvtitledialog);
        title.setTypeface(boldFont);
        title.setText(R.string.dialog_title);

        TextView message = (TextView) dialog.findViewById(R.id.txvdialog);
        message.setTypeface(regularFont);
        message.setTextColor(Color.BLACK);
        message.setText(R.string.dialog_logout);

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.btndialogok);
        dialogButtonOk.setTextColor(Color.BLACK);
        dialogButtonOk.setTypeface(boldFont);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gaszenDB = GaszenDB_DTO.getInstance(getApplicationContext());
                try {
                    if (gaszenDB.updateSession(true) == 1) {
                        AmazonSharedPreferencesWrapper.wipe(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
                        Intent intent = new Intent(getApplicationContext(), Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        dialog.dismiss();
                        Toast.makeText(v.getContext(), R.string.dialog_accept, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Home2.this, R.string.dialog_session_error, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    Log.e("Error: ", e.toString());
                    Toast.makeText(Home2.this, R.string.dialog_session_error, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });

        Button dialogButtonDecline = (Button) dialog.findViewById(R.id.btndialogdecline);
        dialogButtonDecline.setTextColor(Color.BLACK);
        dialogButtonDecline.setTypeface(boldFont);

        dialogButtonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onLoginSuccess(Response success) {

    }

    @Override
    public void onRequestSuccess(String data, short id) {

    }

    @Override
    public void onPostSuccess(String serverResponse) {

    }

    @Override
    public void onPostException(String error) {

    }

    @Override
    public void onBackPressed() {
//        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//            getSupportFragmentManager().popBackStack();
//        } else {
////            super.onBackPressed();
//        }
    }

    public void replaceFragmentWithAnimation(android.support.v4.app.Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (tag.equals("oder_info_tag")) {
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        } else {
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                    R.anim.enter_from_right, R.anim.exit_to_left);
        }
        transaction.replace(R.id.content_home2, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void showPermissionDialog() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);

    }

}
