package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

/**
 * Created by carlosrodriguez on 4/6/17.
 */

public class OrderData {

    private Order order;
    private Client client;
    private Tank tank;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
}
