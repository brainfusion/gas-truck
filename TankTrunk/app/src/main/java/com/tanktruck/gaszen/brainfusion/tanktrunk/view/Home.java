package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Typeface boldFont = null, regularFont = null;
    private Toolbar toolbar;
    public static boolean openPreferences = false;
    private final String TAG = "Home";
    private static int currentFragment = 1;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        boldFont = Typeface.createFromAsset(this.getAssets(), "Nexa-Bold.otf");
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(this.getAssets(), "LatoRegular.ttf");

        //Initializing toolbar and setting is as actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initializing Drawer Layout and ActionBarToggle
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Set the starting fragment
        showFragment(currentFragment);

        setNavItemCount(R.id.nav_alerts, 90);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle of navigation view item clicks.
        int id = item.getItemId();

        if (id == R.id.nav_orders) {
            toolbar.setTitle(getString(R.string.title_activity_home));
            showFragment(1);
            currentFragment = 1;
//        } else if (id == R.id.nav_add_client) {
//            toolbar.setTitle(getString(R.string.add_client));
//            showFragment(2);
//            currentFragment = 2;
//        } else if (id == R.id.nav_add_service) {
//            toolbar.setTitle(getString(R.string.add_service));
//            showFragment(3);
//            currentFragment = 3;
        } else if (id == R.id.nav_status) {
            toolbar.setTitle(getString(R.string.title_status));
            showFragment(4);
            currentFragment = 4;
        } else if (id == R.id.nav_alerts) {
            toolbar.setTitle(getString(R.string.title_alerts_fragment));
            showFragment(5);
            currentFragment = 5;
        } else if (id == R.id.nav_about) {
            toolbar.setTitle(getString(R.string.title_about_fragment));
            showFragment(6);
            currentFragment = 6;
        } else if (id == R.id.nav_logout) {
            toolbar.setTitle(getString(R.string.title_logout));
            setNavItemCount(R.id.nav_alerts, 5);
            showFragment(7);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showFragment(int position) {
        try {
            // Update the main content by replacing fragments
            Fragment fragment = null;
            switch (position) {
                case 1:
                    fragment = new Orders();
                    break;
                case 2:
                    fragment = new RegisterUser();
                    break;
                case 3:
                    fragment = new RegisterService();
                    break;
                case 4:
                    fragment = new DeviceStatus();
                    break;
                case 5:
                    fragment = new Alerts();
                    break;
                case 6:
                    fragment = new About();
                    break;
                case 7:
                    android.app.FragmentManager fm = getFragmentManager();
                    CustomDialog cd = new CustomDialog();
                    cd.show(fm, "Logout");
                    break;
                default:
                    position = 1;
                    break;
            }
            if (fragment != null) {
                if (position != 1) {
                    openPreferences = true;
                }
                invalidateOptionsMenu();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_home, fragment).addToBackStack(null).commit();
            } else {
                Log.e("Error  ", "Mostrar Fragment" + position);
            }
        } catch (Exception e) {
            Log.e(TAG, "showFragment" + e.toString());
        }
    }

    public void setNavItemCount(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        view.setTypeface(boldFont);
        int sum = view.getText() != "" ? Integer.parseInt(view.getText().toString()) : 0;

        sum = sum + count;
        if (sum > 99) {
            view.setTextSize(10);
        }
        else{
            view.setTextSize(14);
        }
        view.setText(sum > 0 ? String.valueOf(sum) : null);
        //float size = view.getTextSize();
    }
}
