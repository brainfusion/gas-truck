package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;

/**
 * Created by carlosrodriguez on 2/17/17.
 */

public interface CloudSyncCallback {
    public void onLoginSuccess(Response success);
    public void onRequestSuccess(String data, short id);
    public void onPostSuccess(String serverResponse);
    public void onPostException(String error);
}
