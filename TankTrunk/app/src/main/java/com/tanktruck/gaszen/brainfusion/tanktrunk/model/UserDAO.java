package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

/**
 * Created by carlosrodriguez on 2/24/17.
 */

public class UserDAO {

    private int idUser=-1;
    private String nameUser="";
    private String email="";
    private String password="";
    private String tokenFb="";
    private String phoneNumber="";
    private short acceptTermAndConditions=0;

    public void addUser(String nameUser, String email, String password, String phoneNumber, short acceptTermAndConditions) {
        if(acceptTermAndConditions>0 && !"".equals(nameUser) && !"".equals(email) && !"".equals(password)){
            setNameUser(nameUser);
            setEmail(email);
            setPassword(password);
            setPhoneNumber(phoneNumber);
            setAcceptTermAndConditions(acceptTermAndConditions);
            //userDAO=getInstance();
            //userDAO.setNameUser(this.nameUser);
            //userDAO.set
        }else{
            //lanzar una excepcion
        }

    }

    public void addUser(int idUser,String nameUser, String email, String password, String phoneNumber, short acceptTermAndConditions) {
        if(acceptTermAndConditions>0 && !"".equals(nameUser) && !"".equals(email) && !"".equals(password)){
            setIdUser(idUser);
            setNameUser(nameUser);
            setEmail(email);
            setPassword(password);
            setPhoneNumber(phoneNumber);
            setAcceptTermAndConditions(acceptTermAndConditions);
            //userDAO=getInstance();
            //userDAO.setNameUser(this.nameUser);
            //userDAO.set
        }else{
            //lanzar una excepcion
        }

    }

    public int getIdUser() {
        return idUser;
    }

    private void setIdUser(int idUser) {
        //userDAO
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    private void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public short getAcceptTermAndConditions() {
        return acceptTermAndConditions;
    }

    private void setAcceptTermAndConditions(short acceptTermAndConditions) {
        this.acceptTermAndConditions = acceptTermAndConditions;
    }
}
