package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlosrodriguez on 1/19/17.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private Typeface boldFont = null, regularFont = null;
    private List<Client> mDataset;
    private List<Client> displayedList;
    private View reference;
    private int position;
    private RecyclerView recyclerView;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView txvListName, txvListAddress, txvName, txvPhone, txvQuantity, txvCoin, txvLiter, txvPrice;
        public TextView txvNameAnon, txvPhoneAnon, txvAddress;
        public Button btnAccept, btnSearch;
        public EditText edtCoin, edtLiter, edtSearch, edtAddressAnon;

        // Create an instance of the dialog fragment
        public ViewHolder(View v) {
            super(v);
            txvListName = (TextView) v.findViewById(R.id.txvusername);
            txvListAddress = (TextView) v.findViewById(R.id.txvuseraddress);
            txvName = (TextView) reference.findViewById(R.id.txv_username);
            txvAddress = (TextView) reference.findViewById(R.id.txv_address);
            txvPhone = (TextView) reference.findViewById(R.id.txv_phone);
            txvNameAnon = (TextView) reference.findViewById(R.id.txv_username_anon);
            edtAddressAnon = (EditText) reference.findViewById(R.id.edt_address_anon);
            txvPhoneAnon = (TextView) reference.findViewById(R.id.txv_phone_anon);
            txvQuantity = (TextView) reference.findViewById(R.id.txv_quatity);
            txvCoin = (TextView) reference.findViewById(R.id.txv_coin);
            txvLiter = (TextView) reference.findViewById(R.id.txv_liter);
            txvPrice = (TextView) reference.findViewById(R.id.txv_price);

            btnAccept = (Button) reference.findViewById(R.id.button_accept);
            btnSearch = (Button) reference.findViewById(R.id.button_search);
            recyclerView = (RecyclerView) reference.findViewById(R.id.recycler_users);

            edtCoin = (EditText) reference.findViewById(R.id.edt_coin);
            edtLiter = (EditText) reference.findViewById(R.id.edt_liter);
            edtSearch = (EditText) reference.findViewById(R.id.edtsearch);

            edtSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    // filter your list from your input
                    filter(s.toString());
                    recyclerView.setVisibility(View.VISIBLE);
                }
            });


            ImageView imgRefresh = (ImageView) reference.findViewById(R.id.imv_refresh);
            imgRefresh.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    txvNameAnon.setText(R.string.service_name_anon);
                    edtAddressAnon.setText("");
                    txvPhoneAnon.setText(R.string.service_phone_anon);
                    edtCoin.setText("");
                    edtLiter.setText("");
                    edtSearch.setText("");
                }
            });

            //Sets On Click Listener for the view and both imageviews
            v.setOnClickListener(this);
        }

        //On Click Listener for view
        @Override
        public void onClick(View v) {
            position = Integer.valueOf(getAdapterPosition());
            //Toast.makeText(v.getContext(), "ROW PRESSED = " + position, Toast.LENGTH_SHORT).show();
            Client temp = displayedList.get(position);
            //txvNameAnon.setText(temp.getName());
            //edtAddressAnon.setText(temp.getAddress() + " " + temp.getCol());
            //txvPhoneAnon.setText(temp.getPostal_code());
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ServicesAdapter(List<Client> myDataset, View v) {
        mDataset = myDataset;
        displayedList = myDataset;
        reference = v;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_users, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ServicesAdapter.ViewHolder vh = new ServicesAdapter.ViewHolder(v);
        boldFont = Typeface.createFromAsset(v.getContext().getAssets(), "Nexa-Bold.otf");
        regularFont = Typeface.createFromAsset(v.getContext().getAssets(), "NexaRegular.otf");
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ServicesAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Client temp = displayedList.get(position);
        //holder.txvListName.setText(temp.getName());
        //holder.txvListAddress.setText(temp.getAddress() + " " + temp.getCol());

        holder.txvListName.setTypeface(boldFont);
        holder.txvListAddress.setTypeface(boldFont);
        holder.txvName.setTypeface(regularFont);
        holder.txvAddress.setTypeface(regularFont);
        holder.txvPhone.setTypeface(regularFont);
        holder.txvNameAnon.setTypeface(regularFont);
        holder.edtAddressAnon.setTypeface(regularFont);
        holder.txvPhoneAnon.setTypeface(regularFont);
        holder.btnAccept.setTypeface(boldFont);
        holder.edtCoin.setTypeface(regularFont);
        holder.edtLiter.setTypeface(regularFont);
        holder.txvCoin.setTypeface(regularFont);
        holder.txvQuantity.setTypeface(regularFont);
        holder.txvLiter.setTypeface(regularFont);
        holder.txvPrice.setTypeface(regularFont);
        holder.edtSearch.setTypeface(regularFont);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return displayedList.size();
    }

    private void updateList(List<Client> list){
        displayedList = list;
        notifyDataSetChanged();
    }

    private void filter(String text){
        List<Client> temp = new ArrayList();
        if (text.isEmpty()){
            updateList(temp);
        }
        else {
            text = text.toLowerCase();
            for(Client d: mDataset){
                String name = "", address = "";
                //name = d.getName();
                //address = d.getAddress() + " " + d.getCol();
                name = name.toLowerCase();
                address = address.toLowerCase();
                if(name.contains(text.toLowerCase())){
                    temp.add(d);
                }
                else if(address.contains(text.toLowerCase())){
                    temp.add(d);
                }
            }
            //update recyclerview
            updateList(temp);
        }
    }
}
