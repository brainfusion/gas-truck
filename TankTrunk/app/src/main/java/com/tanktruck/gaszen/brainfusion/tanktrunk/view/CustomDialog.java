package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;

/**
 * Created by thomasepp on 2017-01-26.
 */

public class CustomDialog extends DialogFragment {

    private Typeface regularFont = null, boldFont = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");
        View v = inflater.inflate(R.layout.custom_dialog, container, false);


        TextView txvTitle = (TextView) v.findViewById(R.id.txvtitledialog);
        TextView txvDialog = (TextView) v.findViewById(R.id.txvdialog);
        Button btnTrue = (Button) v.findViewById(R.id.btndialogok);
        Button btnFalse = (Button) v.findViewById(R.id.btndialogdecline);
        ImageView iv = (ImageView) v.findViewById(R.id.icondialog);
        iv.setImageResource(R.mipmap.ic_about);

        txvTitle.setTypeface(boldFont);
        txvDialog.setTypeface(regularFont);
        btnTrue.setTypeface(boldFont);
        btnFalse.setTypeface(boldFont);

        txvTitle.setText(R.string.logout_title);
        txvDialog.setText(R.string.logout_message);
        btnTrue.setText(R.string.logout_accept);
        btnFalse.setText(R.string.logout_cancel);

        btnTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), R.string.logout_accept, Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
                Intent intent = new Intent(getActivity(), Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), R.string.logout_cancel, Toast.LENGTH_SHORT).show();
                //dismiss();
                getDialog().dismiss();
            }
        });

        return v;
    }
}
