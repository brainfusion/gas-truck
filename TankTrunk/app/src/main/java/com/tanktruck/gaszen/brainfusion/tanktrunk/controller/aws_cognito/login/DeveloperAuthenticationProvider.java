package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.amazonaws.auth.AWSAbstractCognitoDeveloperIdentityProvider;
import com.amazonaws.regions.Regions;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.session_credentials.Utilities;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;
import com.tanktruck.gaszen.brainfusion.tanktrunk.model.UserDAO;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;


/**
 * Created by erickleonel on 22/12/2015.
 */
public class DeveloperAuthenticationProvider extends AWSAbstractCognitoDeveloperIdentityProvider {
    private Context context;
    private static final String developerProvider = "login.gaszen.provideruser";
    private static AmazonCognitoSampleDeveloperAuthenticationClient devAuthClient;
    //private static final String GASZEN_AUTHENTICATION_CLIENT_APP_ENDPOINT = "https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenUserLogin";
    private static final String GASZEN_AUTHENTICATION_CLIENT_APP_ENDPOINT = "https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenProviderBranchTruckOperatorLogin";
    /*For Facebook User Register/Login*/
    private static final String EXTERNAL_AUTHENTICATION_CLIENT_APP_ENDPOINT = " https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenUserLoginExternal";
    private static final String cognitoSampleDeveloperAuthenticationAppName = "GaszenUsers";
    private static CloudSyncCallback cloudSyncCallback = null;
    private GaszenDB_DTO gaszenDB = null;
    private String TAG = "DeveloperAuthentication";

    public DeveloperAuthenticationProvider(String accountId, String identityPoolId, Context context, Regions region, short typeSession) {
        super(accountId, identityPoolId, region);
        this.context = context;
        if (developerProvider == null || developerProvider.isEmpty()) {
            Log.e(TAG, "Error: developerProvider name not set!");
            throw new RuntimeException("DeveloperAuthenticatedApp not configured.");
        }

        URL endpoint;
        try {
            if (typeSession == 0) {
                endpoint = new URL(GASZEN_AUTHENTICATION_CLIENT_APP_ENDPOINT);
                Log.d(TAG, "SESSION TYPE=0: " + endpoint);
            } else {
                if (typeSession == 1) {
                    endpoint = new URL(EXTERNAL_AUTHENTICATION_CLIENT_APP_ENDPOINT);
                    Log.d(TAG, "SESSION TYPE=1: " + endpoint);
                } else {
                    endpoint = null;
                    Log.d(TAG, "SESSION TYPE=0: " + endpoint);
                }
            }
        } catch (MalformedURLException e) {
            Log.e("DeveloperAuthentication", "Developer Authentication Endpoint is not a valid URL!", e);
            throw new RuntimeException(e);
        }
        /*
         * Initialize the client using which you will communicate with your
         * backend for user authentication. Here we initialize a client which
         * communicates with sample Cognito developer authentication
         * application.
         */
        devAuthClient = new AmazonCognitoSampleDeveloperAuthenticationClient(
                context,endpoint, cognitoSampleDeveloperAuthenticationAppName);
    }

    @Override
    public String getProviderName() {
        Log.d("AWS", "@getProviderName");
        return developerProvider;
    }

    @Override
    public String getIdentityId() {
        // return super.getIdentityId();
        Log.d("AWS", "@getIdentityId");
        identityId = CognitoSyncClientManager.credentialsProvider.getCachedIdentityId();
        if (identityId == null) {
            if (getProviderName() != null && !this.loginsMap.isEmpty() && this.loginsMap.containsKey(getProviderName())) {
                Log.d("AWS", "UPDATE VALUES: IdeD/CognitoCachingCredentialsProvider: Clearing credentials from SharedPreferencesntityId: " + AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager
                        .getDefaultSharedPreferences(context)) + " | Token: " + AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager
                        .getDefaultSharedPreferences(context)));
                update(AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager
                                .getDefaultSharedPreferences(context)),
                        AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager
                                .getDefaultSharedPreferences(context)));

                return AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager
                        .getDefaultSharedPreferences(context));
            } else {
               try {
                Log.d("AWS", "IdentityId-re-uth(1else)");
                UserDAO user = null;
                gaszenDB = GaszenDB_DTO.getInstance(context);
                    int idUser=gaszenDB.getSession().getIdUser();
                    user = gaszenDB.getUser(idUser);
                    processLogin(user.getEmail(), user.getPassword(), gaszenDB.getSession().getType());

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager
                        .getDefaultSharedPreferences(context));
            }
        } else {
            try {
            Log.d("AWS", "IdentityId-re-uth(2else)");
            UserDAO user = null;
            gaszenDB = GaszenDB_DTO.getInstance(context);
                int idUser=gaszenDB.getSession().getIdUser();
                user = gaszenDB.getUser(idUser);
                processLogin(user.getEmail(), user.getPassword(), gaszenDB.getSession().getType());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager
                    .getDefaultSharedPreferences(context));
        }
    }

    @Override
    public String refresh() {
        Log.d("AWS", "@refresh");
        // Override the existing token
        setToken(null);
        if (getProviderName() != null && !this.loginsMap.isEmpty() && this.loginsMap.containsKey(getProviderName())) {
            Log.d("AWS", "INTO IF - REAUNTHENTICATE PROCESS");
            CognitoSyncClientManager.credentialsProvider
                    .clearCredentials();
            UserDAO user = null;
            try {
                gaszenDB = GaszenDB_DTO.getInstance(context);
                int idUser=gaszenDB.getSession().getIdUser();
                user = gaszenDB.getUser(idUser);
                processLogin(user.getEmail(), user.getPassword(), gaszenDB.getSession().getType());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager
                    .getDefaultSharedPreferences(context));
        } else {
            Log.d("AWS", "INTO ELSE -GET iDENTITYid");
            this.getIdentityId();
            return null;
        }
    }

    /**
     * This function validates the user credentials against the sample Cognito
     * developer authentication application. After that it stores the key and
     * token received from sample Cognito developer authentication application
     * for all further communication with the application.
     *
     * @param userName
     * @param password
     */
    public void login(String userName, String password, CloudSyncCallback cloudSyncCallback, Context context, short typeSession) {
        Log.d("AWS", "login()");
        //this.cloudSyncCallback=cloudSyncCallback;
        new DeveloperAuthenticationTask(context, cloudSyncCallback).execute(new LoginCredentials(userName, password, typeSession));
    }

    public void processLogin(String user, String pass, short typeSession) {
        Log.d("AWS", "processLOgin() ");
        final String AWS_DEVICE_UID = "AWS_DEVICE_UID";
        final String AWS_DEVICE_TOKEN = "AWS_DEVICE_TOKEN";
        boolean isSuccessful;
        String userName;
        Response response = DeveloperAuthenticationProvider.getDevAuthClientInstance().login(user, pass, typeSession);
        String token = "";
        String identityId = "";
        Log.d("AWS", " RESPONSE: " + response.getResponseMessage()+" User: "+user);

        isSuccessful = response.requestWasSuccessful();
        userName = user;

        if (isSuccessful && Utilities.getToken(response.getResponseMessage()) != null) {//Checar como validar, ya que retorna el OpenTokenId!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            CognitoSyncClientManager.addLogins(((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider
                            .getIdentityProvider()).getProviderName(),
                    userName);
            token = Utilities.getToken(response.getResponseMessage());
            identityId = Utilities.getIdentityId(response.getResponseMessage());
            //Save Token in Shared Preferences
            AmazonSharedPreferencesWrapper.storeValueInSharedPreferences(PreferenceManager
                    .getDefaultSharedPreferences(context), AWS_DEVICE_TOKEN, token);
            AmazonSharedPreferencesWrapper.storeValueInSharedPreferences(PreferenceManager
                    .getDefaultSharedPreferences(context), AWS_DEVICE_UID, identityId);

            if (cloudSyncCallback != null) {
                cloudSyncCallback.onLoginSuccess(response);
            }

            CognitoSyncClientManager.addLogins(((DeveloperAuthenticationProvider) CognitoSyncClientManager.credentialsProvider
                            .getIdentityProvider()).getProviderName(),
                    response.getResponseMessage());
        }else{
            //Could not login success
        }
    }

    public static AmazonCognitoSampleDeveloperAuthenticationClient getDevAuthClientInstance() {
        if (devAuthClient == null) {
            Log.d("AWS", "Dev Auth Client not initialized yet");
            throw new IllegalStateException(
                    "Dev Auth Client not initialized yet");
        }
        return devAuthClient;
    }
}

