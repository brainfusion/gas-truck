/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * <p>
 * http://aws.amazon.com/apache2.0
 * <p>
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * A utility class for communicating with sample Cognito developer
 * authentication application.
 */
public class CognitoSampleDeveloperAuthenticationService {
    private static final String LOG_TAG = "CognitoSampleDeveloperAuthenticationService";
    private static final String ERROR = "Internal Server Error";


    /**
     * A function to send request to the sample Cognito developer authentication
     * application
     *
     * @param request
     * @param reponseHandler
     * @return
     */
    public static Response sendRequest(Context context, Request request, ResponseHandler reponseHandler, short typeSession) {
        int responseCode = 0;
        String[] params;
        String responseBody = null;
        String requestUrl = null;
        String value = null;
        SSLContext sslContext = null;
        TrustManager tm = null;
        DataOutputStream dataOutputStream = null;
        StringBuilder serverResponse = null;
        String line = "";

        try {
            params = request.buildRequestUrlParams(context, typeSession);
            requestUrl = params[0];
            value = params[1];

            Log.d("AWS", "Sending Request : [" + requestUrl + "]");
            Log.d("AWS", "Parameters : [" + value + "]");

            sslContext = SSLContext.getInstance("TLS");
            tm = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            sslContext.init(null, new TrustManager[]{tm}, new SecureRandom());

            URL url = new URL(requestUrl);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            //connection.setRequestProperty("Content-Type","application/json");
            //connection.setRequestProperty("Host", "https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com");
            //connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            //Next two headers are necessary for the https request executes properly
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("x-api-key", "pgTfTuNxnAahff8VDybxA5MhN1Z5jxlE1h2R1AdU");
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(8000);//10 seconds in milliseconds
            connection.setReadTimeout(10000);//15 seconds in milliseconds
            connection.connect();

            dataOutputStream = new DataOutputStream(connection.getOutputStream());
            dataOutputStream.writeBytes(value);
            dataOutputStream.flush();
            dataOutputStream.close();

            responseCode = connection.getResponseCode();
            Log.d(LOG_TAG, "HTTPS RESPONSE CODE: " + responseCode);
            if (responseCode == 200) {
                serverResponse = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = bufferedReader.readLine()) != null) {
                    serverResponse.append(line);
                }
                bufferedReader.close();
                responseBody = serverResponse.toString();
                Log.d(LOG_TAG, "Response : [" + responseBody + "]");
            } else {
                Log.d("AWS", "Response : [" + connection.getErrorStream() + "]");
                connection.getErrorStream();
            }
            //responseBody = CognitoSampleDeveloperAuthenticationService.getResponse(connection);
            return reponseHandler.handleResponse(responseCode, responseBody);

        } catch (IOException exception) {
            Log.w(LOG_TAG, exception);
            if (exception.getMessage().equals("Received authentication challenge is null")) {
                return reponseHandler.handleResponse(401, "Unauthorized token request");
            } else {
                return reponseHandler.handleResponse(404, "Unable to reach resource at [" + requestUrl + "]");
            }
        } catch (Exception exception) {
            Log.e(LOG_TAG, exception.getMessage());
            exception.printStackTrace();
            return reponseHandler.handleResponse(responseCode, responseBody);
        }
    }
}
