package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by carlosrodriguez on 1/20/17.
 */

public class Client {
    private Integer id_client;
    private String client_name;
    private String client_street_address;
    private String client_neighborhood_address;
    private String client_city;
    private Integer client_ext_number;
    private Integer client_int_number;
    private Integer client_postal_code;
    private Double client_lat;
    private Double client_lng;


    /*public Client(String Name, String street_Address, String neighborhood_Address, String City, Integer ext_Number, Integer int_Number, Integer postal_Code){

    }*/

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_street_address() {
        return client_street_address;
    }

    public void setClient_street_address(String client_street_address) {
        this.client_street_address = client_street_address;
    }

    public String getClient_neighborhood_address() {
        return client_neighborhood_address;
    }

    public void setClient_neighborhood_address(String client_neighborhood_address) {
        this.client_neighborhood_address = client_neighborhood_address;
    }

    public String getClient_city() {
        return client_city;
    }

    public void setClient_city(String client_city) {
        this.client_city = client_city;
    }

    public Integer getClient_ext_number() {
        return client_ext_number;
    }

    public void setClient_ext_number(Integer client_ext_number) {
        this.client_ext_number = client_ext_number;
    }

    public Integer getClient_int_number() {
        return client_int_number;
    }

    public void setClient_int_number(Integer client_int_number) {
        this.client_int_number = client_int_number;
    }

    public Integer getClient_postal_code() {
        return client_postal_code;
    }

    public void setClient_postal_code(Integer client_postal_code) {
        this.client_postal_code = client_postal_code;
    }

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }


    public Double getClient_lat() {
        return client_lat;
    }

    public void setClient_lat(Double client_lat) {
        this.client_lat = client_lat;
    }

    public Double getClient_lng() {
        return client_lng;
    }

    public void setClient_lng(Double client_lng) {
        this.client_lng = client_lng;
    }
}
