package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.aws_lambda;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.AWSSessionCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.LogType;
import com.amazonaws.services.securitytoken.model.ExpiredTokenException;
import com.google.gson.JsonObject;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.session_credentials.SessionCredential;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by erickleonel on 29/12/2015.
 */
public class LambdaManager {
    private AWSLambdaClient lambdaClient = null;
    private Context context;
    private InvokeRequest invokeRequest = null;
    private InvokeResult invokeResult = null;
    private LambdaFunctionExecutor lambdaFunctionExecuter = null;
    private CloudSyncCallback cloudSyncCallback = null;
    private CloudSyncCallback cloudSyncCallbackDeviceDataUpdate = null;
    private final String TAG = "LambdaUpdateDataService";

    public LambdaManager(Context context) {
        this.context = context;
    }

    public boolean GetOrders(String data, short id, CloudSyncCallback cloudSyncCallback) {
        try {
            this.cloudSyncCallback = cloudSyncCallback;
            final String functionName = "arn:aws:lambda:us-east-1:817944992172:function:gaszenPBTOListOrders";
            lambdaFunctionExecuter = new LambdaFunctionExecutor();
            lambdaFunctionExecuter.execute(functionName, data,""+id);
            lambdaFunctionExecuter.get(20000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (ExecutionException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (TimeoutException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        }
        return true;
    }

    public boolean UpdateOrderStatus(String data, short id, CloudSyncCallback cloudSyncCallback) {
        try {
            this.cloudSyncCallback = cloudSyncCallback;
            final String functionName = "arn:aws:lambda:us-east-1:817944992172:function:gaszenPBTOUpdateOrderStatus";
            lambdaFunctionExecuter = new LambdaFunctionExecutor();
            lambdaFunctionExecuter.execute(functionName, data,""+id);
            lambdaFunctionExecuter.get(20000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (ExecutionException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (TimeoutException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        }
        return true;
    }

    public boolean RegisterClient(String data, short id, CloudSyncCallback cloudSyncCallback) {
        try {
            this.cloudSyncCallback = cloudSyncCallback;
            final String functionName = "arn:aws:lambda:us-east-1:817944992172:function:gaszenPBTORegisterClient";
            lambdaFunctionExecuter = new LambdaFunctionExecutor();
            lambdaFunctionExecuter.execute(functionName, data,""+id);
            lambdaFunctionExecuter.get(20000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (ExecutionException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (TimeoutException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        }
        return true;
    }

    public boolean RegisterService(String data, short id, CloudSyncCallback cloudSyncCallback) {
        try {
            this.cloudSyncCallback = cloudSyncCallback;
            final String functionName = "arn:aws:lambda:us-east-1:817944992172:function:gaszenPBTORegisterService";
            lambdaFunctionExecuter = new LambdaFunctionExecutor();
            lambdaFunctionExecuter.execute(functionName, data,""+id);
            lambdaFunctionExecuter.get(20000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (ExecutionException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        } catch (TimeoutException e) {
            lambdaFunctionExecuter.cancel(true);
            this.cloudSyncCallback.onRequestSuccess("-31",id);
            e.printStackTrace();
        }
        return true;
    }

    /**
     * A AsyncTask class that execute in background all functions in Amazon
     */
    private class LambdaFunctionExecutor extends AsyncTask<String, Void, String[]> {
        SessionCredential sessionCredential = null;
        short id=0;
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String[] doInBackground(String... params) {
            sessionCredential = SessionCredential.getInstance(context);
            String[] args = null;
            id= Short.parseShort(params[2]);
            try {
                Log.d(TAG, "Antes de la credenciales: ");
                AWSSessionCredentials credentials = sessionCredential.getSessionCredential(false);
                Log.d(TAG, "Despues de la credenciales: ");
                lambdaClient = new AWSLambdaClient(credentials);
                lambdaClient.setRegion(Region.getRegion(Regions.US_EAST_1));

                invokeRequest = new InvokeRequest();
                invokeRequest.setInvocationType(InvocationType.RequestResponse);
                invokeRequest.setLogType(LogType.Tail);
                invokeRequest.setFunctionName(params[0]);
                Log.d(TAG, ".........:::::::::::::Function name: " + params[0].toString());

                //Map<String, String> payload = new HashMap<>();
                //payload.put("entry_value", params[1]);

                JSONObject payload = new JSONObject();

                try {
                    if (params[1].toString().isEmpty()){
                        payload.put("entry_value", "");
                    }
                    else {
                        JSONObject data = new JSONObject(params[1]);
                        payload.put("entry_value", data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "frame : " + params[1]);
                invokeRequest.setPayload(ByteBuffer.wrap(payload.toString().getBytes()));

                invokeResult = lambdaClient.invoke(invokeRequest);
                Log.d(TAG, "Lambda Result: " + invokeResult);
                Log.d(TAG, "Lambda Status code: " + invokeResult.getStatusCode());
                Log.d(TAG, "Lambda FunctionError: " + invokeResult.getFunctionError());
                Log.d(TAG, ": " + invokeResult.getPayload());
                String v = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
                Log.d(TAG, "Lambda Payload(String): " + v);
                args = new String[1];
                args[0] = v.toString();
            } catch (ExpiredTokenException e) {
                Log.i(TAG, "Lambda Manager: doInBack Exception(Open Token Id Exception): " + e.toString());
                Log.i(TAG, "Service Name: : " + e.getServiceName());
                if ("AWSSecurityTokenService".equals(e.getServiceName())) {
                    sessionCredential.getSessionCredential(true);
                    args = new String[2];
                    args[0] = params[0];
                    args[1] = params[1];
                }
            } catch (com.amazonaws.AmazonServiceException e) {
                Log.i(TAG, "Lambda Manager: doInBack Exception(Session Credentials Exception): " + e.toString());
                Log.i(TAG, "Service Name: : " + e.getServiceName());
                if ("AWSLambda".equals(e.getServiceName())) {
                    SessionCredential.awsSessionCredentials = null;
                    sessionCredential.getSessionCredential(false);
                    args = new String[2];
                    args[0] = params[0];
                    args[1] = params[1];
                } else {
                    if ("AWSSecurityTokenService".equals(e.getServiceName())) {
                        args = new String[1];
                        args[0] = "402";
                    }
                }
            } catch (com.amazonaws.AmazonClientException e) {
                Log.e(TAG, "Error: " + e.toString());
                args = new String[1];
                args[0] = "-31";
            }
            return args;
        }

        @Override
        protected void onPostExecute(String[] args) {
            Log.d(TAG, "onPostExecute()");
            if (args != null) {
                if (args.length == 1) {
                    cloudSyncCallback.onRequestSuccess(args[0],id);
                } else if (args.length == 2) {
                    cloudSyncCallback.onRequestSuccess("-20",id);
                }
            } else {

            }
        }
    }
}