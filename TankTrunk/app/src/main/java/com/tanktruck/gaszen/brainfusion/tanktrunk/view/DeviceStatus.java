package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.events.DecoEvent;


/**
 * Created by thomasepp on 2017-01-13.
 */

public class DeviceStatus extends android.support.v4.app.Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Typeface regularFont = null, boldFont = null;
        int graphColorTank = Color.argb(255, 64, 196, 0);
        int graphColorBattery = Color.argb(255, 64, 196, 0);
        int graphColorOrders = Color.argb(255, 64, 196, 0);
        int backgroundColor = Color.argb(255, 218, 218, 218);
        int appBackgroundColor = Color.argb(255, 244, 244, 248);

        View v = inflater.inflate(R.layout.fragment_device_status, container, false);

        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");

        float batteryPercent = getBatteryLevel();

        final TextView textViewTankLvl = (TextView) v.findViewById(R.id.txv_tank_lvl);
        final TextView textViewBatteryLvl = (TextView) v.findViewById(R.id.txv_battery_lvl);
        final TextView textViewDailyOrders = (TextView) v.findViewById(R.id.txv_status_daily_orders);
        TextView textViewGPSStatus = (TextView) v.findViewById(R.id.txv_status_gps);
        TextView textViewPrinterStatus = (TextView) v.findViewById(R.id.txv_printer_status);
        TextView textViewConnection = (TextView) v.findViewById(R.id.txv_status_connection);
        TextView textViewTankLvlText = (TextView) v.findViewById(R.id.txv_tank_lvl_text);
        TextView textViewBatteryText = (TextView) v.findViewById(R.id.txv_battery_lvl_text);
        TextView textViewDelStatText = (TextView) v.findViewById(R.id.txv_status_daily_orders_text);
        TextView textViewStatusConnection = (TextView) v.findViewById(R.id.txv_status_connection_status);
        TextView textViewStatusPrinter = (TextView) v.findViewById(R.id.txv_status_printer_status);
        TextView textViewStatusGps = (TextView) v.findViewById(R.id.txv_status_gps_status);
        ImageView imageViewConnection = (ImageView) v.findViewById(R.id.imageViewConnection);
        ImageView imageViewGps = (ImageView) v.findViewById(R.id.imageViewGPS);
        ImageView imageViewPrinter = (ImageView) v.findViewById(R.id.imageViewPrinter);


        textViewTankLvl.setTypeface(regularFont);
        textViewBatteryLvl.setTypeface(regularFont);
        textViewGPSStatus.setTypeface(regularFont);
        textViewPrinterStatus.setTypeface(regularFont);
        textViewConnection.setTypeface(regularFont);
        textViewDailyOrders.setTypeface(regularFont);
        textViewTankLvlText.setTypeface(regularFont);
        textViewBatteryText.setTypeface(regularFont);
        textViewDelStatText.setTypeface(regularFont);
        textViewStatusConnection.setTypeface(regularFont);
        textViewStatusPrinter.setTypeface(regularFont);
        textViewStatusGps.setTypeface(regularFont);


        //Series Item for background of each graph
        SeriesItem seriesItemBackground = new SeriesItem.Builder(appBackgroundColor)
                .setRange(99, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .setInterpolator(new LinearInterpolator())
                .setShowPointWhenEmpty(false)
                .setInset(new PointF(100f, 100f))
                .setCapRounded(false)
                .setDrawAsPoint(false)
                .setSpinClockwise(true)
                .setSpinDuration(6000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();


        DecoView arcView = (DecoView) v.findViewById(R.id.dynamicArcView_tank_level);
        arcView.configureAngles(180, 0);
        int tankPercentage = 50;

        if (tankPercentage >= 50) {
            graphColorTank = Color.argb(255, 64, 196, 0);
        } else if (tankPercentage < 50 && tankPercentage > 20) {
            graphColorTank = Color.argb(255, 255, 255, 51);
        } else if (graphColorTank < 20) {
            graphColorTank = Color.argb(255, 255, 0, 0);
        }
        // Create background track
        arcView.addSeries(new SeriesItem.Builder(backgroundColor)
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .build());

        //Create data series track
        final SeriesItem seriesItem1 = new SeriesItem.Builder(graphColorTank)
                .setRange(0, 100, 0)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(false)
                .setDrawAsPoint(false)
                .setInset(new PointF(2f, 2f))
                .setSpinClockwise(true)
                .setSpinDuration(7000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();

        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                textViewTankLvl.setText((String.format("%.0f%%", percentFilled * 100f)));

            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        int series1Index = arcView.addSeries(seriesItem1);
        int seriesBackgroundIndex = arcView.addSeries(seriesItemBackground);


        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        arcView.addEvent(new DecoEvent.Builder(tankPercentage).setIndex(series1Index).setDelay(1000).build());
        //arcView.addEvent(new DecoEvent.Builder(100).setIndex(series11Index).setDuration(0).build());

        arcView.addEvent(new DecoEvent.Builder(110).setIndex(seriesBackgroundIndex).build());


        //Creating Statistic chart
        DecoView dailyLvl = (DecoView) v.findViewById(R.id.dynamicArcView_delivery_status);
        dailyLvl.configureAngles(180, 0);

        int completedOrders = 49;

        if (completedOrders >= 50) {
            graphColorOrders = Color.argb(255, 64, 196, 0);
        } else if (completedOrders < 50 && completedOrders > 20) {
            graphColorOrders = Color.argb(255, 255, 255, 51);
        } else if (graphColorTank < 20) {
            graphColorOrders = Color.argb(255, 255, 0, 0);
        }


        dailyLvl.addSeries(new SeriesItem.Builder(backgroundColor)
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .build());


        final SeriesItem completedDelivery = new SeriesItem.Builder(graphColorOrders)
                .setRange(0, 100, 0)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(false)
                .setInset(new PointF(2f, 2f))
                .setDrawAsPoint(false)
                .setSpinClockwise(true)
                .setSpinDuration(7000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();

        completedDelivery.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilledOrders = ((currentPosition - completedDelivery.getMinValue()) / (completedDelivery.getMaxValue() - completedDelivery.getMinValue()));
                textViewDailyOrders.setText((String.format("%.0f%%", percentFilledOrders * 100f)));

            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });


        int seriesIndexDailyComplete = dailyLvl.addSeries(completedDelivery);
        seriesBackgroundIndex = dailyLvl.addSeries(seriesItemBackground);

        dailyLvl.addEvent(new DecoEvent.Builder(completedOrders).setIndex(seriesIndexDailyComplete).setDelay(1000).build());
        dailyLvl.addEvent(new DecoEvent.Builder(110).setIndex(seriesBackgroundIndex).build());

        //Creating Battery % chart
        DecoView batteryLvl = (DecoView) v.findViewById(R.id.dynamicArcView_batteryPercentage);
        batteryLvl.configureAngles(180, 0);

        batteryLvl.addSeries(new SeriesItem.Builder(backgroundColor)
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .build());

        if (batteryPercent >= 50) {
            graphColorBattery = Color.argb(255, 64, 196, 0);
        } else if (batteryPercent < 50 && batteryPercent >= 20) {
            graphColorBattery = Color.argb(255, 255, 255, 51);
        } else if (batteryPercent < 20) {
            graphColorBattery = Color.argb(255, 255, 0, 0);
        }

        final SeriesItem seriesItemBattery = new SeriesItem.Builder(graphColorBattery)
                .setRange(0, 100, 0)
                .setInitialVisibility(false)
                .setLineWidth(32f)
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(false)
                .setDrawAsPoint(false)
                .setInset(new PointF(2f, 2f))
                .setSpinClockwise(true)
                .setSpinDuration(6000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();

        seriesItemBattery.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilledBattery = ((currentPosition - seriesItemBattery.getMinValue()) / (seriesItemBattery.getMaxValue() - seriesItemBattery.getMinValue()));
                textViewBatteryLvl.setText((String.format("%.0f%%", percentFilledBattery * 100f)));

            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        int seriesIndexBattery = batteryLvl.addSeries(seriesItemBattery);
        seriesBackgroundIndex = batteryLvl.addSeries(seriesItemBackground);

        batteryLvl.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        batteryLvl.addEvent(new DecoEvent.Builder(batteryPercent).setIndex(seriesIndexBattery).setDelay(1000).build());
        batteryLvl.addEvent(new DecoEvent.Builder(110).setIndex(seriesBackgroundIndex).build());

        Context context = this.getContext();

        //Check if GPS is active/inactive
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            textViewStatusGps.setText(R.string.lvl_active);
            imageViewGps.setImageResource(R.drawable.gps);
        } else {
            textViewStatusGps.setText(R.string.lvl_inactive);
            imageViewGps.setImageResource(R.drawable.gps_false);
        }

        /*
        //Get Wifi Status
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
            textViewStatusConnection.setText(R.string.lvl_active);
        } else {
            textViewStatusConnection.setText(R.string.lvl_inactive);
        }*/


        if (getConnectionStatus()) {
            imageViewConnection.setImageResource(R.drawable.data_signal);
            textViewStatusConnection.setText(R.string.lvl_active);
        } else if (!getConnectionStatus()) {
            imageViewConnection.setImageResource(R.drawable.no_data_signal);
            textViewStatusConnection.setText(R.string.lvl_inactive);
        }


        boolean printerLvl = true;

        if (printerLvl) {
            imageViewPrinter.setImageResource(R.drawable.printer);
        } else if (!printerLvl) {
            imageViewPrinter.setImageResource(R.drawable.printer_false_3);
        }

        return v;
    }

    //Method checks battery level
    private float getBatteryLevel() {
        IntentFilter batteryIntent = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.getContext().registerReceiver(null, batteryIntent);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        return ((float) level);
    }

    //Get the current state of connection
    private boolean getConnectionStatus() {
        boolean conn = false;
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                conn = true;
            }
        } else if (activeNetwork == null) {
            conn = false;
        }

        return conn;
    }

}

