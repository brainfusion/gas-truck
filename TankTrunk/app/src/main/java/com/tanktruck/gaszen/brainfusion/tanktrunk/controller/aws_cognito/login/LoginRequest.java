/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * <p/>
 * http://aws.amazon.com/apache2.0
 * <p/>
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login;


import android.content.Context;
import android.util.Log;

import com.amazonaws.util.HttpUtils;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;

import org.json.JSONObject;

import java.net.URL;


/**
 * This class is used to construct the Login request for communication with
 * sample Cognito developer authentication.
 */
public class LoginRequest extends Request { //It possible to change facebook data frame

    private final URL endpoint;
    private final String uid;
    private final String username;
    private final String password;
    private final String appName;
    private final String TAG="LoginRequest";

    public LoginRequest(final URL endpoint, final String appName,
                        final String uid, final String username, final String password) {
        this.endpoint = endpoint;
        this.appName = appName;
        this.uid = uid;
        this.username = username;
        this.password = password;
    }

    /*
     * (non-Javadoc)
     * @see com.amazonaws.cognito.sync.devauth.client.Request#buildRequestUrl()
     */
    @Override
    public String[] buildRequestUrlParams(Context context, short typeSession) {
        try {
            String url = this.endpoint.toString();
            StringBuilder builder = null;
            JSONObject json = new JSONObject();
            JSONObject json2 = new JSONObject();
            String asd = null;

            if (typeSession == 0) {
                //json.put("email", HttpUtils.urlEncode(username, true));
                //json.put("password", HttpUtils.urlEncode(password, true));
                //The request has to be sent in json format
                json.put("email", username);
                json.put("password", password);
                json2.put("entry_value", json);
                /*builder = new StringBuilder("entry_value={" +
                        "'email':'" + HttpUtils.urlEncode(username, false) + "'," +
                        "'password':'" + HttpUtils.urlEncode(password, false) + "'}");*/

                Log.d(TAG, "TIPE SESSION= 0 || FRAME: " + json2.toString());
            } else {
                if (typeSession == 1) {
                    GaszenDB_DTO gaszenDB = GaszenDB_DTO.getInstance(context);
                    String name = "";
                    /*name = gaszenDB.getUserByEmail(username).getNameUser();*/
                    builder = new StringBuilder("entry_value={" +
                            "'email':'" + HttpUtils.urlEncode(username, false) + "'," +
                            "'name_user':'" + HttpUtils.urlEncode(name, false) + "'," +
                            "'external_provider_name':'graph.facebook.com'," +
                            "'external_provider_token':'" + HttpUtils.urlEncode(password, false) + "'}");

                    Log.d(TAG, "TIPE SESSION= 1 || FRAME: " + json2.toString());
                } else {

                }
            }
            String params[] = {url, json2.toString()};
            return params;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String buildRequestUrl() {
        return null;
    }

}
