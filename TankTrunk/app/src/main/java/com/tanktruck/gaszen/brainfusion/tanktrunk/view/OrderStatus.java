package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.events.DecoEvent;

public class OrderStatus extends AppCompatActivity {

    private Typeface regularFont = null, boldFont = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getAssets(), "Nexa-Bold.otf");

        final float mSeriesMax = 100f;
        final float mSeriesMin = 0f;
        float mSeriesPercent = 78;

        final TextView txvPercernt = (TextView) findViewById(R.id.txv_orderStatus_percentage);


        DecoView statusChart = (DecoView) findViewById(R.id.dynamicArcView_order_status);

        statusChart.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                .setRange(mSeriesMin, mSeriesMax, mSeriesMax)
                .setInitialVisibility(true)
                .setLineWidth(32f)
                .build());

        final SeriesItem seriesOrderStatusItem = new SeriesItem.Builder(Color.argb(255, 64, 196, 0))
                .setRange(mSeriesMin, mSeriesMax, mSeriesMin)
                .setInitialVisibility(true)
                .setCapRounded(true)
                .setInterpolator(new OvershootInterpolator())
                .setShowPointWhenEmpty(false)
                .setCapRounded(true)
                .setInset(new PointF(32f, 32f))
                .setDrawAsPoint(false)
                .setSpinClockwise(true)
                .setSpinDuration(6000)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_PIE)
                .build();

        seriesOrderStatusItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesOrderStatusItem.getMinValue()) / (seriesOrderStatusItem.getMaxValue() - seriesOrderStatusItem.getMinValue()));
                txvPercernt.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        int mSeriesIndexBackground = statusChart.addSeries(seriesOrderStatusItem);


        //statusChart.configureAngles(180, 0);

        statusChart.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        statusChart.addEvent(new DecoEvent.Builder(mSeriesPercent).setIndex(mSeriesIndexBackground).setDelay(1000).build());


    }
}
