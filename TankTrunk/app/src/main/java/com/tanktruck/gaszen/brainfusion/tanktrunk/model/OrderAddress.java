package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

/**
 * Created by carlosrodriguez on 4/8/17.
 */

public class OrderAddress {
    private Integer id_order;
    private Integer id_client;
    private String client_street_address;
    private String client_neighborhood_address;

    /*public OrderAddress(Integer id_Order, Integer id_Client, String client_Street_Address, String client_Neighborhood_Address){
        setId_order(id_Order);
        setId_client(id_Client);
        setClient_street_address(client_Street_Address);
        setClient_neighborhood_address(client_Neighborhood_Address);
    }*/

    public Integer getId_order() {
        return id_order;
    }

    public void setId_order(Integer id_order) {
        this.id_order = id_order;
    }

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }

    public String getClient_street_address() {
        return client_street_address;
    }

    public void setClient_street_address(String client_street_address) {
        this.client_street_address = client_street_address;
    }

    public String getClient_neighborhood_address() {
        return client_neighborhood_address;
    }

    public void setClient_neighborhood_address(String client_neighborhood_address) {
        this.client_neighborhood_address = client_neighborhood_address;
    }
}
