package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by thomasepp on 2017-01-23.
 */

public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        //Changed regular font to LatoRegular
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "LatoRegular.ttf");
        setTypeface(tf);
    }
}
