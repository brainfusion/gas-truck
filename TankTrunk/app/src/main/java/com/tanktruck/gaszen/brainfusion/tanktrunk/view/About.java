package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;

/**
 * Created by thomasepp on 2017-01-06.
 */

public class About extends android.support.v4.app.Fragment {

    private Typeface regularFont = null, boldFont = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View about = inflater.inflate(R.layout.fragment_about, container, false);
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");

        TextView txvVersion = (TextView) about.findViewById(R.id.txv_about_version);
        TextView txvUrl = (TextView) about.findViewById(R.id.txv_about_url);
        TextView txvRegistered = (TextView) about.findViewById(R.id.txv_about_registered);

        txvVersion.setTypeface(regularFont);
        txvUrl.setTypeface(regularFont);
        txvRegistered.setTypeface(regularFont);

        return about;
    }
}