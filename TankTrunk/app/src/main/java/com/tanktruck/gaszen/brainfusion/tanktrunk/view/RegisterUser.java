package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.management.GaszenManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by thomasepp on 2017-01-11.
 */

public class RegisterUser extends android.support.v4.app.Fragment implements CloudSyncCallback {

    private Typeface regularFont = null, boldFont = null;
    private GaszenManager gaszenManager = null;
    private EditText edtName;
    private EditText edtFLName;
    private EditText edtSLName;
    private EditText edtPhone;
    private EditText edtEmail;
    private EditText edtAddress;
    private EditText edtCol;
    private EditText edtCity;
    private EditText edtZipCode;
    private EditText edtExtNum;
    private EditText edtIntNum;
    private EditText edtCountry;
    private EditText edtLocality;
    private Spinner spinnState;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View vSpinn = inflater.inflate(R.layout.spinner_item, container, false);
        TextView edtSpinner = (TextView) vSpinn.findViewById(R.id.edt_spinner_item);
        edtSpinner.setTypeface(regularFont);

        View RegisterUser = inflater.inflate(R.layout.fragment_register_user, container, false);

        gaszenManager = new GaszenManager(getContext());

        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), "Nexa-Bold.otf");

        TextInputLayout tilName = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_name);
        edtName = (EditText) RegisterUser.findViewById(R.id.edt_regusr_name);
        tilName.setHint(getString(R.string.usr_name));

        TextInputLayout tilFstLastName = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_fst_lastname);
        edtFLName = (EditText) RegisterUser.findViewById(R.id.edt_regusr_first_last_name);
        tilFstLastName.setHint(getString(R.string.usr_firstLastName));


        TextInputLayout tilSndLastName = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_snd_lastname);
        edtSLName = (EditText) RegisterUser.findViewById(R.id.edt_regusr_second_last_name);
        tilSndLastName.setHint(getString(R.string.usr_secondLastName));

        TextInputLayout tilPhone = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_phone);
        edtPhone = (EditText) RegisterUser.findViewById(R.id.edt_regusr_phone);
        tilPhone.setHint(getString(R.string.usr_phoneNumber));

        TextInputLayout tilEmail = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_email);
        edtEmail = (EditText) RegisterUser.findViewById(R.id.edt_regusr_email);
        tilEmail.setHint(getString(R.string.usr_email));

        TextInputLayout tilStreet = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_street);
        edtAddress = (EditText) RegisterUser.findViewById(R.id.edt_regusr_address);
        tilStreet.setHint(getString(R.string.usr_street));

        TextInputLayout tilNeighbor = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_col);
        edtCol = (EditText) RegisterUser.findViewById(R.id.edt_regusr_colony);
        tilNeighbor.setHint(getString(R.string.usr_colony));

        TextInputLayout tilCity = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_city);
        edtCity = (EditText) RegisterUser.findViewById(R.id.edt_regusr_city);
        tilCity.setHint(getString(R.string.usr_city));

        TextInputLayout tilCountry = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_country);
        edtCountry = (EditText) RegisterUser.findViewById(R.id.edt_regusr_country);
        tilCountry.setHint(getString(R.string.usr_Country));

        TextInputLayout tilZipCode = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_zip_code);
        edtZipCode = (EditText) RegisterUser.findViewById(R.id.edt_regusr_zip_code);
        tilZipCode.setHint(getString(R.string.usr_zipCode));

        TextInputLayout tilExtNum = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_ext_num);
        edtExtNum = (EditText) RegisterUser.findViewById(R.id.edt_regusr_num);
        tilExtNum.setHint(getString(R.string.usr_street_ext));

        TextInputLayout tilIntNum = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout);
        edtIntNum = (EditText) RegisterUser.findViewById(R.id.edt_regusr_int_num);
        tilIntNum.setHint(getString(R.string.usr_street_int));

        TextInputLayout tilLocality = (TextInputLayout) RegisterUser.findViewById(R.id.text_input_layout_locality);
        edtLocality = (EditText) RegisterUser.findViewById(R.id.edt_regusr_locality);
        tilLocality.setHint(getString(R.string.usr_locality));

        //Working Spinner
        spinnState = (Spinner) RegisterUser.findViewById(R.id.spin_regusr_state);
        String[] myResArray = getResources().getStringArray(R.array.state_names);
        List<String> states = new ArrayList<>();
        states.addAll(Arrays.asList(myResArray));
        final ArrayAdapter<String> statesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, states) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {

                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(getResources().getColor(R.color.gaszen_blue2));
                    tv.setTextSize(14);
                } else {
                    tv.setTextColor(getResources().getColor(R.color.gaszen_text_black2));
                    tv.setTextSize(20);
                }
                return view;
            }

        };
        spinnState.setAdapter(statesAdapter);


        edtName.setTypeface(regularFont);
        edtFLName.setTypeface(regularFont);
        edtSLName.setTypeface(regularFont);
        edtPhone.setTypeface(regularFont);
        edtEmail.setTypeface(regularFont);
        edtAddress.setTypeface(regularFont);
        edtCol.setTypeface(regularFont);
        edtCity.setTypeface(regularFont);
        edtZipCode.setTypeface(regularFont);
        edtExtNum.setTypeface(regularFont);
        edtIntNum.setTypeface(regularFont);
        edtCountry.setTypeface(regularFont);
        edtLocality.setTypeface(regularFont);

        Button btnRegister = (Button) RegisterUser.findViewById(R.id.btn_regusr_register);
        btnRegister.setTypeface(boldFont);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    boolean flagCon = checkConnectivity();
                    if (flagCon){
                        JSONObject json = new JSONObject();
                        json.put("city", edtCity.getText().toString());
                        json.put("country", edtCountry.getText().toString());
                        json.put("ext_number", edtExtNum.getText().toString());
                        json.put("int_number", edtIntNum.getText().toString());
                        json.put("locality", edtLocality.getText().toString());
                        json.put("neighborhood", edtCol.getText().toString());
                        json.put("state", spinnState.getSelectedItem().toString());
                        json.put("street_name", edtAddress.getText().toString());
                        json.put("zip_postal_code", edtZipCode.getText().toString());
                        json.put("email", edtEmail.getText().toString());
                        json.put("name", edtName.getText().toString());
                        json.put("last_name", edtFLName.getText().toString() + " " + edtSLName.getText().toString());
                        json.put("phone_number", edtPhone.getText().toString());
                        Long tsLong = System.currentTimeMillis()/1000;
                        String ts = tsLong.toString();
                        json.put("timestamp", ts);
                        gaszenManager.RegisterClient(json.toString(), (short) 0, RegisterUser.this);
                    }
                    else {
                        Toast.makeText(getContext(), R.string.toast_connection_error, Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e){
                    Toast.makeText(getContext(), R.string.toast_register_user_error, Toast.LENGTH_LONG).show();
                }
            }
        });

        return RegisterUser;
    }

    private boolean checkConnectivity() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            //Toast.makeText(this, getString(R.string.abc_message_no_intenet), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onLoginSuccess(Response success) {

    }

    @Override
    public void onRequestSuccess(String data, short id) {
        try {
            if (id == 0){
                Log.d("JSON", data);
                String format = data.replace("\\", "");
                format = format.substring(1, format.length() - 1);
                Log.d("JSON", "Data FORMAT: " + format);
                JSONObject jsonData = new JSONObject(format);
                int resultCode = Integer.parseInt(jsonData.getString("result_code"));
                if (resultCode != 200){
                    Toast.makeText(getContext(), R.string.toast_register_user_error, Toast.LENGTH_LONG).show();
                }
                else if (resultCode == 200){

                    edtName.setText("");
                    edtFLName.setText("");
                    edtSLName.setText("");
                    edtPhone.setText("");
                    edtEmail.setText("");
                    edtAddress.setText("");
                    edtCol.setText("");
                    edtCity.setText("");
                    edtZipCode.setText("");
                    edtExtNum.setText("");
                    edtIntNum.setText("");
                    edtCountry.setText("");
                    edtLocality.setText("");
                    spinnState.setSelection(0);
                    edtName.requestFocus();

                    Toast.makeText(getContext(), R.string.toast_register_user_success, Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostSuccess(String serverResponse) {

    }

    @Override
    public void onPostException(String error) {

    }
}
