package com.tanktruck.gaszen.brainfusion.tanktrunk.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.*;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tanktruck.gaszen.brainfusion.tanktrunk.R;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.CognitoSyncClientManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.Response;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.management.GaszenManager;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.persistence.GaszenDB_DTO;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity implements CloudSyncCallback {

    private Typeface regularFont = null, boldFont = null;
    private ImageView img;
    private GaszenManager gaszenManager = null;
    private GaszenDB_DTO db = null;
    private ProgressDialog dialog = null;
    private Context context = this;

    private Dialog customDialog;
    private Button dialogLeftButton;
    private Button dialogRightButton;
    private TextView dialogTitle;
    private TextView dialogBody;
    private ImageView dialogImage;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = GaszenDB_DTO.getInstance(this);
        gaszenManager = new GaszenManager(this);


        //Assings the specified font to constants and then sets the font to the UI items
        //Changed regular font to LatoRegular
        regularFont = Typeface.createFromAsset(getAssets(), "LatoRegular.ttf");
        boldFont = Typeface.createFromAsset(getAssets(), "Nexa-Bold.otf");
        final EditText edtUser = (EditText) findViewById(R.id.txtNameUser);
        edtUser.setTypeface(regularFont);
        final EditText edtPassword = (EditText) findViewById(R.id.txtPassword);
        edtPassword.setTypeface(regularFont);
        Button btnLogin = (Button) findViewById(R.id.btnLogIn);
        btnLogin.setTypeface(boldFont);
        TextView lblForgotPassword = (TextView) findViewById(R.id.lblForgotPassword);
        lblForgotPassword.setTypeface(boldFont);


        //Custom Dialog Components
        customDialog = new Dialog(this);
        customDialog.setContentView(R.layout.custom_alert_dialog);
        dialogLeftButton = (Button) customDialog.findViewById(R.id.btnAlertLeft);
        dialogLeftButton.setTypeface(boldFont);
        dialogRightButton = (Button) customDialog.findViewById(R.id.btnAlertRight);
        dialogRightButton.setTypeface(boldFont);
        dialogTitle = (TextView) customDialog.findViewById(R.id.txtAlertTitle);
        dialogTitle.setTypeface(boldFont);
        dialogBody = (TextView) customDialog.findViewById(R.id.txtAlertBody);
        dialogBody.setTypeface(regularFont);
        dialogImage = (ImageView) customDialog.findViewById(R.id.iconAlertDialog);
        dialogImage.setImageResource(R.mipmap.ic_alert);
        dialogRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });


        // Load the ImageView that will host the animation and
        // set its background to our AnimationDrawable XML resource.
        img = (ImageView) findViewById(R.id.animation);
        img.setBackgroundResource(R.drawable.stove_animation);

        //Set checkbox On Checked Change Listener
        CheckBox chkbxShowPass = (CheckBox) findViewById(R.id.chkbxShowPass);
        chkbxShowPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EditText edtPassword = (EditText) findViewById(R.id.txtPassword);
                if (isChecked) {
                    edtPassword.setTransformationMethod(null);
                } else if (!isChecked) {
                    edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = checkConnectivity();
                if (flag) {
                    String user = edtUser.getText().toString();
                    String password = edtPassword.getText().toString();

                    Boolean validEmail = isValidEmail(user);
                    Boolean validPassword = isValidPassword(password);
                    CognitoSyncClientManager.init(getApplicationContext(), (short) 0);
                    if (validEmail && validPassword) {
                        send(user, password);
                        dialog = ProgressDialog.show(context,
                                getString(R.string.progress_dialog_title_login),
                                getString(R.string.progress_dialog_body_login), true);
                    } else {
                        if (!validEmail) {
                            edtUser.requestFocus();
                        } else {
                            edtPassword.requestFocus();
                        }
                        Toast.makeText(v.getContext(),
                                R.string.toast_email_validation,
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(v.getContext(),
                            R.string.toast_connection_error,
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    /**
     * This method is called whenever the Activity becomes visible or invisible to the user.
     * During this method call its possible to start the animation.
     */
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
        if (hasFocus) {
            frameAnimation.start();
        } else {
            frameAnimation.stop();
        }
    }

    private boolean checkConnectivity() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if ((info == null || !info.isConnected() || !info.isAvailable())) {
            return false;
        }
        return true;
    }

    private void send(final String user, final String pass) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                gaszenManager.initializeUserSession(user, pass, Login.this, (short) 0);
            }
        }).start();
    }

    /**
     * Email format validation
     **/
    public static boolean isValidEmail(CharSequence usr) {
        return !TextUtils.isEmpty(usr) && android.util.Patterns.EMAIL_ADDRESS.matcher(usr).matches();
    }

    /**
     * Password validation
     **/
    public static boolean isValidPassword(CharSequence pwd) {
        return !TextUtils.isEmpty(pwd);
    }

    @Override
    public void onLoginSuccess(Response success) {
        try {
            JSONObject jsonObject = new JSONObject(success.getResponseMessage());
            Integer responseCode = Integer.parseInt(jsonObject.getString("result_code"));

            switch (responseCode) {
                case 200:
                    dialog.setTitle(getString(R.string.progress_dialog_get_data_title));
                    dialog.setMessage(getString(R.string.progress_dialog_get_data_body));
                    EditText user = (EditText) findViewById(R.id.txtNameUser);
                    EditText pass = (EditText) findViewById(R.id.txtPassword);
                    String name = jsonObject.getString("operator_name");
                    Integer res = db.swipeUserSession(user.getText().toString());
                    if (res == 0) {
                        db.saveUserSession(name, user.getText().toString(), pass.getText().toString());
                    }
                    Intent intent = new Intent(this, Home2.class);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                    break;
                case 5039:
                    dialog.dismiss();
                    dialogLeftButton.setText("");
                    dialogRightButton.setText(R.string.alert_close);
                    dialogTitle.setText(R.string.alert_user_not_found_title);
                    dialogBody.setText(R.string.alert_user_not_found_body);
                    customDialog.show();
                    break;
                case 6003:
                    dialog.dismiss();
                    dialogLeftButton.setText("");
                    dialogRightButton.setText(R.string.alert_close);
                    dialogTitle.setText(R.string.alert_title_credentials_error);
                    dialogBody.setText(R.string.toast_credentials_error);
                    customDialog.show();
                    break;
                case 6004:
                    dialog.dismiss();
                    dialogLeftButton.setText("");
                    dialogRightButton.setText(R.string.alert_close);
                    dialogTitle.setText(R.string.alert_cognito_exception_title);
                    dialogBody.setText(R.string.alert_cognito_exception);
                    customDialog.show();
                    System.out.println("Cognito exception error");
                    break;
                default:
                    dialog.dismiss();
                    dialogLeftButton.setText("");
                    dialogRightButton.setText(R.string.alert_close);
                    dialogTitle.setText(R.string.alert_default_title);
                    dialogBody.setText(R.string.toast_login_error);
                    customDialog.show();
                    System.out.println("Login error");
                    break;

            }
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }
//        TextView test = (TextView) findViewById(R.id.txvPostTest);
//        test.setText(success.getResponseMessage());
    }

    @Override
    public void onRequestSuccess(String data, short id) {

    }

    @Override
    public void onPostSuccess(String serverResponse) {
        //Toast.makeText(this, serverResponse, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPostException(String error) {

    }
}
