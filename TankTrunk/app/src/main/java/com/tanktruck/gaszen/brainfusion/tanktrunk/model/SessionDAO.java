package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

import android.util.Log;

/**
 * Created by carlosrodriguez on 2/24/17.
 */

public class SessionDAO {
    private int idSession=-1;
    private int idUser=-1;
    private short stateSession=-1;
    private short type=-1;
//    private int percentageAlert;
//    private int alertType;

    public void addSession(int idSession, int idUser, short stateSession, short type){
        if(idSession>0 && idUser>0 && stateSession>0 && type>=0 ){
            setIdSession(idSession);
            setIdUser(idUser);
            setStateSession(stateSession);
            setType(type);
        }else{
        }
    }

    public void addSession(int idUser, short stateSession, short type){
        if(idUser>0 && stateSession>=0 && type>=0 ){
            setIdUser(idUser);
            setStateSession(stateSession);
            setType(type);
        }else{
            Log.d("SESSION","ELSE");
        }
    }

    public int getIdSession() {
        return idSession;
    }

    private void setIdSession(int idSession) {
        this.idSession = idSession;
    }

    public int getIdUser() {
        return idUser;
    }

    private void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public short getStateSession() {
        return stateSession;
    }

    public void setStateSession(short stateSession) {
        this.stateSession = stateSession;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
}
