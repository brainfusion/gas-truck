package com.tanktruck.gaszen.brainfusion.tanktrunk.model;

/**
 * Created by carlosrodriguez on 3/23/17.
 */

public class Order {
    private Integer id_order;
    private String qty_liters;
    private String payment_method;
    private String sale_status;
    private String price_per_liter;
    private String currency;
    private String total_sale;
    private String timestamp;
    private Integer id_client;
    private Integer id_tank;
    private String device_uuid;
    private String order_id;
    private String payment_method_authorization;
    private String email;
    private String sale_payment_status;
    private String payment_method_id;

    /*public Order (Integer id_Order, String qty_Liters, String payment_Method, String sale_Status, String price_Per_Liter, String Currency, String total_Sale, String Timestamp, Integer id_Client, Integer id_Tank){
        setId_order(id_Order);
        setQty_liters(qty_Liters);
        setPayment_method(payment_Method);
        setSale_status(sale_Status);
        setPrice_per_liter(price_Per_Liter);
        setCurrency(Currency);
        setTotal_sale(total_Sale);
        setTimestamp(Timestamp);
        setId_client(id_Client);
        setId_tank(id_Tank);
    }*/

    public String getQty_liters() {
        return qty_liters;
    }

    public void setQty_liters(String qty_liters) {
        this.qty_liters = qty_liters;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getSale_status() {
        return sale_status;
    }

    public void setSale_status(String sale_status) {
        this.sale_status = sale_status;
    }

    public String getPrice_per_liter() {
        return price_per_liter;
    }

    public void setPrice_per_liter(String price_per_liter) {
        this.price_per_liter = price_per_liter;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal_sale() {
        return total_sale;
    }

    public void setTotal_sale(String total_sale) {
        this.total_sale = total_sale;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }

    public Integer getId_tank() {
        return id_tank;
    }

    public void setId_tank(Integer id_tank) {
        this.id_tank = id_tank;
    }

    public Integer getId_order() {
        return id_order;
    }

    public void setId_order(Integer id_order) {
        this.id_order = id_order;
    }

    public String getDevice_uuid() {
        return device_uuid;
    }

    public void setDevice_uuid(String device_uuid) {
        this.device_uuid = device_uuid;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPayment_method_authorization() {
        return payment_method_authorization;
    }

    public void setPayment_method_authorization(String payment_method_authorization) {
        this.payment_method_authorization = payment_method_authorization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSale_payment_status() {
        return sale_payment_status;
    }

    public void setSale_payment_status(String sale_payment_status) {
        this.sale_payment_status = sale_payment_status;
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }
}
