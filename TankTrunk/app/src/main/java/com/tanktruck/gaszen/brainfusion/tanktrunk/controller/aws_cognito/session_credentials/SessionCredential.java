package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.session_credentials;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.amazonaws.auth.AWSSessionCredentials;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.AssumeRoleWithWebIdentityRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleWithWebIdentityResult;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.ExpiredTokenException;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.AmazonSharedPreferencesWrapper;
import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.aws_cognito.login.DeveloperIdentity;

/**
 * Created by erickleonel on 22/12/2015.
 */
public class SessionCredential {
    private static String tokenForDeveloperIdentity;
    private static SessionCredential sessionCredential = null;
    public static AWSSessionCredentials awsSessionCredentials = null;
    private Context context;
    private AWSSecurityTokenService stsClient;
    private AssumeRoleWithWebIdentityRequest stsRequest;
    private AssumeRoleWithWebIdentityResult stsResponse;
    private DeveloperIdentity developerIdentity;
    private String TAG = "SessionCredential";


    //private static final String ROLE_ARN = "arn:aws:iam::817944992172:role/Cognito_GaszenUsersAuth_Role";
    private static final String ROLE_ARN = "arn:aws:iam::817944992172:role/Cognito_ProviderBranchTruckOperatorAuth_Role";

    public SessionCredential(Context context) {
        this.context = context;
    }

    public static synchronized SessionCredential getInstance(Context context) {
        if (sessionCredential == null) {
            sessionCredential = new SessionCredential(context);
        }
        return sessionCredential;
    }

    public AWSSessionCredentials getSessionCredential(boolean openTokenIdExpired) throws ExpiredTokenException, com.amazonaws.AmazonServiceException { //tratar excepcion de token expirado
        try {
            Log.d(TAG, "getSessionCredential()");
            if (awsSessionCredentials != null && openTokenIdExpired == false) {
                Log.i(TAG, "awsSessionCredentials != null && openTokenIdExpired ==false");
                return awsSessionCredentials;
            } else {
                Log.d(TAG, "getSessionCredential2()");
                tokenForDeveloperIdentity = AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager.getDefaultSharedPreferences(context));
                if (tokenForDeveloperIdentity == null || openTokenIdExpired) {
                    Log.i(TAG, "(getSessionCredentials)re-authentication process... OpenTokenExpired: " + openTokenIdExpired);
                    //Put code of re-authentication function
                    Log.i(TAG, "Antes de Open Token");
                    developerIdentity = new DeveloperIdentity(context);
                    developerIdentity.getOpenIdToken(true);
                    Log.i(TAG, "DESPUES DE oOPnen token");

                    stsClient = new AWSSecurityTokenServiceClient(new AnonymousAWSCredentials());
                    stsRequest = new AssumeRoleWithWebIdentityRequest();
                    stsRequest.setRoleArn(ROLE_ARN);
                    tokenForDeveloperIdentity = AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager.getDefaultSharedPreferences(context));
                    stsRequest.setWebIdentityToken(tokenForDeveloperIdentity);
                    stsRequest.setRoleSessionName("android.gaszen.app");
                    //stsReq.setPolicy();
                    stsRequest.setDurationSeconds(3600); //The duration, in seconds, of the role session. The value can range from 900 seconds (15 minutes) to 3600 seconds (1 hour). By default, the value is set to 3600 seconds.
                    stsResponse = stsClient.assumeRoleWithWebIdentity(stsRequest);
                    Credentials stsCredentials = stsResponse.getCredentials();
                    awsSessionCredentials = new BasicSessionCredentials(
                            stsCredentials.getAccessKeyId(),
                            stsCredentials.getSecretAccessKey(),
                            stsCredentials.getSessionToken());
                    Log.i(TAG, "##############################################CREDENTIALS####################################################################");
                    Log.i(TAG, "ACCESS KEY ID: " + awsSessionCredentials.getAWSAccessKeyId());
                    Log.i(TAG, "SECRET ACCESS KEY: " + awsSessionCredentials.getAWSSecretKey());
                    Log.i(TAG, "SESSION TOKEN: " + awsSessionCredentials.getSessionToken());
                    Log.i(TAG, "DATE EXPIRATION : " + stsCredentials.getExpiration());

                } else {
                    Log.d(TAG, "(getSessionCredentials)else...");
                    Log.d(TAG, "OPEN TOKEN IDENTITY="+ AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager.getDefaultSharedPreferences(context)));
                    Log.d(TAG, "OPEN DEVICE ID="+ AmazonSharedPreferencesWrapper.getUidForDevice(PreferenceManager.getDefaultSharedPreferences(context)));
                    stsClient = new AWSSecurityTokenServiceClient(new AnonymousAWSCredentials());
                    stsRequest = new AssumeRoleWithWebIdentityRequest();
                    stsRequest.setRoleArn(ROLE_ARN);
                    stsRequest.setWebIdentityToken(tokenForDeveloperIdentity);
                    stsRequest.setRoleSessionName("android.gaszen.app");
                    stsRequest.setDurationSeconds(3600); //The duration, in seconds, of the role session. The value can range from 900 seconds (15 minutes) to 3600 seconds (1 hour). By default, the value is set to 3600 seconds.
                    stsResponse = stsClient.assumeRoleWithWebIdentity(stsRequest);
                    Credentials stsCredentials = stsResponse.getCredentials();
                    awsSessionCredentials = new BasicSessionCredentials(
                            stsCredentials.getAccessKeyId(),
                            stsCredentials.getSecretAccessKey(),
                            stsCredentials.getSessionToken());
                    Log.i(TAG, "##############################################CREDENTIALS####################################################################");
                    Log.i(TAG, "ACCESS KEY ID: " + awsSessionCredentials.getAWSAccessKeyId());
                    Log.i(TAG, "SECRET ACCESS KEY: " + awsSessionCredentials.getAWSSecretKey());
                    Log.i(TAG, "SESSION TOKEN: " + awsSessionCredentials.getSessionToken());
                    Log.i(TAG, "DATE EXPIRATION : " + stsCredentials.getExpiration());
                }
                return awsSessionCredentials;
            }
        } catch (ExpiredTokenException e) {
            try {
                Log.i(TAG, "Lambda Manager: doInBack Exception(Open Token Id Exception): " + e.toString());
                Log.i(TAG, "Service Name: : " + e.getServiceName());
                if ("AWSSecurityTokenService".equals(e.getServiceName())) {

                    Log.d(TAG, "(getSessionCredentials)re-authentication process... OpenTokenExpired: " + openTokenIdExpired);
                    developerIdentity = new DeveloperIdentity(context);
                    developerIdentity.getOpenIdToken(true);

                    stsClient = new AWSSecurityTokenServiceClient(new AnonymousAWSCredentials());
                    stsRequest = new AssumeRoleWithWebIdentityRequest();
                    stsRequest.setRoleArn(ROLE_ARN);
                    tokenForDeveloperIdentity = AmazonSharedPreferencesWrapper.getTokenForDevice(PreferenceManager.getDefaultSharedPreferences(context));
                    stsRequest.setWebIdentityToken(tokenForDeveloperIdentity);
                    stsRequest.setRoleSessionName("android.gaszen.app");

                    stsRequest.setDurationSeconds(3600); //The duration, in seconds, of the role session. The value can range from 900 seconds (15 minutes) to 3600 seconds (1 hour). By default, the value is set to 3600 seconds.
                    stsResponse = stsClient.assumeRoleWithWebIdentity(stsRequest);
                    Credentials stsCredentials = stsResponse.getCredentials();
                    awsSessionCredentials = new BasicSessionCredentials(
                            stsCredentials.getAccessKeyId(),
                            stsCredentials.getSecretAccessKey(),
                            stsCredentials.getSessionToken());
                    Log.i(TAG, "##############################################CREDENTIALS####################################################################");
                    Log.i(TAG, "ACCESS KEY ID: " + awsSessionCredentials.getAWSAccessKeyId());
                    Log.i(TAG, "SECRET ACCESS KEY: " + awsSessionCredentials.getAWSSecretKey());
                    Log.i(TAG, "SESSION TOKEN: " + awsSessionCredentials.getSessionToken());
                    Log.i(TAG, "DATE EXPIRATION : " + stsCredentials.getExpiration());
                }
            }catch (Exception ex){
                e.printStackTrace();
            }
            return awsSessionCredentials;

        } catch (com.amazonaws.AmazonServiceException e) {
            Log.i(TAG, "Lambda Manager: doInBack Exception(Session Credentials Exception): " + e.toString());
            Log.i(TAG, "Service Name: : " + e.getServiceName());
            if ("AWSLambda".equals(e.getServiceName())) {
                Log.d(TAG, "(getSessionCredentials)else...");
                stsClient = new AWSSecurityTokenServiceClient(new AnonymousAWSCredentials());
                stsRequest = new AssumeRoleWithWebIdentityRequest();
                stsRequest.setRoleArn(ROLE_ARN);
                stsRequest.setWebIdentityToken(tokenForDeveloperIdentity);
                stsRequest.setRoleSessionName("android.gaszen.app");
                stsRequest.setDurationSeconds(3600); //The duration, in seconds, of the role session. The value can range from 900 seconds (15 minutes) to 3600 seconds (1 hour). By default, the value is set to 3600 seconds.
                stsResponse = stsClient.assumeRoleWithWebIdentity(stsRequest);
                Credentials stsCredentials = stsResponse.getCredentials();
                awsSessionCredentials = new BasicSessionCredentials(
                        stsCredentials.getAccessKeyId(),
                        stsCredentials.getSecretAccessKey(),
                        stsCredentials.getSessionToken());
                Log.i(TAG, "##############################################CREDENTIALS####################################################################");
                Log.i(TAG, "ACCESS KEY ID: " + awsSessionCredentials.getAWSAccessKeyId());
                Log.i(TAG, "SECRET ACCESS KEY: " + awsSessionCredentials.getAWSSecretKey());
                Log.i(TAG, "SESSION TOKEN: " + awsSessionCredentials.getSessionToken());
                Log.i(TAG, "DATE EXPIRATION : " + stsCredentials.getExpiration());
            }
            return awsSessionCredentials;
        }
    }
}
