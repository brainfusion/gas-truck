package com.tanktruck.gaszen.brainfusion.tanktrunk.controller.transport;

import android.os.AsyncTask;
import android.util.Log;

import com.tanktruck.gaszen.brainfusion.tanktrunk.controller.callback.CloudSyncCallback;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by carlosrodriguez on 2/17/17.
 */

public class HttpsHandler {

    private StringBuilder _url=null;
    private HttpsPostTask httpsPostTask=null;
    private CloudSyncCallback _postCallback=null;

    public HttpsHandler(CloudSyncCallback postCallback){
        this._postCallback=postCallback;
    }

    public void post(short noFunction, StringBuilder frame) throws NoSuchAlgorithmException, KeyManagementException, MalformedURLException {

        if(noFunction!=-1 && !frame.equals("")) {
            _url=new StringBuilder();
            switch (noFunction) {
                case 0: //User Register
                    _url.append("https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenUserRegistration");
                    httpsPostTask=new HttpsPostTask(_url, frame);
                    httpsPostTask.execute();
                    break;

                case 1://User Facebook Register
                    _url.append("https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenUserLoginExternal");
                    httpsPostTask=new HttpsPostTask(_url, frame);
                    httpsPostTask.execute();
                    break;

                case 2://User Login
                    _url.append("https://v9qqfx49mi.execute-api.us-east-1.amazonaws.com/prod/gaszenUserLogin");
                    httpsPostTask=new HttpsPostTask(_url, frame);
                    httpsPostTask.execute();
                    break;

                default:
                    break;
            }
        }
    }

    private class HttpsPostTask extends AsyncTask<String, Void, String> {
        private URL url = null;
        private StringBuilder urlParameters=null;
        private SSLContext sslContext = null;
        private TrustManager tm = null;
        private HttpsURLConnection secureConnection = null;
        private DataOutputStream dataOutputStream = null;
        private StringBuilder serverResponse = null;

        HttpsPostTask(StringBuilder url, StringBuilder frame) throws NoSuchAlgorithmException, KeyManagementException, MalformedURLException {
            this.url = new URL(url.toString());
            this.urlParameters = frame;
            this.sslContext = SSLContext.getInstance("TLS");
            this.tm = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
                @Override
                public X509Certificate[] getAcceptedIssuers() {return null;}
            };
            sslContext.init(null, new TrustManager[]{tm}, new SecureRandom());
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                int httpsResponseCode;
                String line = "";
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                secureConnection = (HttpsURLConnection) url.openConnection();

                serverResponse = new StringBuilder();
                secureConnection.setRequestMethod("POST");
                secureConnection.setRequestProperty("x-api-key", "pgTfTuNxnAahff8VDybxA5MhN1Z5jxlE1h2R1AdU");
                secureConnection.setConnectTimeout(8000);//10 seconds in milliseconds
                secureConnection.setReadTimeout(10000);//15 seconds in milliseconds
                secureConnection.setDoOutput(true);
                secureConnection.setDoInput(true);
                dataOutputStream = new DataOutputStream(secureConnection.getOutputStream());
                dataOutputStream.writeBytes(urlParameters.toString());

                httpsResponseCode = secureConnection.getResponseCode();
                Log.d("HTTPS", "HTTPS RESPONSE CODE: " + httpsResponseCode);
                if (httpsResponseCode == 200) { //Connection Success
                    //Check HTTPS ResponseCode
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(secureConnection.getInputStream()));
                    while ((line = bufferedReader.readLine()) != null) {
                        serverResponse.append(line);
                    }
                    bufferedReader.close();
                } else {
                    /*No matter the error number only tells that something happened and try again.*/
                    serverResponse.append("H403");
                }
            }catch (Exception e){
                e.printStackTrace();
                Log.e("HTTPS", e.getMessage());
                serverResponse.append("TimeOut");
            }
            return (serverResponse.toString());
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            //Finish Progress Wait Dialog
            //Execute Callback
            Log.d("HTTPS", " _postCallback.onPostSuccess(string):"+string);
            _postCallback.onPostSuccess(string);
        }

        @Override
        protected void onCancelled(String string) {
            _postCallback.onPostException(string);
            super.onCancelled(string);}
    }
}
